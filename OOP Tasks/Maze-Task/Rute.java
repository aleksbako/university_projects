import java.util.ArrayList;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public abstract class Rute extends JButton{
    protected boolean visited;
    protected int column;
    protected int row;
    protected Labyrint labyrint;
    protected Rute northRoute;
    protected Rute southRoute;
    protected Rute eastRoute;
    protected Rute westRoute;
    protected ArrayList<Tuppel> currentPath;
    protected char symbol;

    /**
     *
     * @param row - the row coordinates of the element in maze
     * @param colomn - the column coordinates of the element in maze
     * @param labyrint - the laberyth to which the object belongs to.
     */
    public Rute(int row, int colomn, Labyrint labyrint){
        this.column = colomn;
        this.row = row;
        this.labyrint = labyrint;
        visited = false;

    }

    public void setNorthRoute(Rute northRoute) {
        this.northRoute = northRoute;
    }

    public void setSouthRoute(Rute southRoute) {
        this.southRoute = southRoute;
    }

    public void setEastRoute(Rute eastRoute) {
        this.eastRoute = eastRoute;
    }

    public void setWestRoute(Rute westRoute) {
        this.westRoute = westRoute;
    }

    public abstract String tilTegn();

    public abstract void gaa(ArrayList<Tuppel> path);

    public void finnUtvei(){
        //To avoid edge case that we start at exit which would cause an error to happen.
        if(column == 0 || column == labyrint.getRoutes()[0].length-1 || row == 0 || row == labyrint.getRoutes().length-1) {
            System.out.println("Starting at an edge, please try different coordinates.");
        }
        else {
            currentPath = new ArrayList<>();
            this.gaa(currentPath);

        }
    }

    void initRoute () {
        setBorder(BorderFactory.createLineBorder(Color.black));
        setFont(new Font("Monospaced", Font.BOLD, 50));
        setPreferredSize(new Dimension(80, 80));
        setText(" ");

        Rute denneRuten = this;

        class ChoosePath implements ActionListener {
            @Override
            public void actionPerformed (ActionEvent e) {
                ArrayList<ArrayList<Tuppel>> temp = labyrint.finnUtveiFra(denneRuten.column, denneRuten.row);
                if (temp.size() != 0) {
                    labyrint.updatePathlist(temp);
                    for (int i = 0; i < temp.get(0).size(); i++) {
                        labyrint.setPath(temp.get(0).get(i));
                        System.out.println(temp.get(0).get(i).getColumn() + "," + temp.get(0).get(i).getRow());

                    }

                }

            }
        }
        addActionListener(new ChoosePath());
    }

    char getSymbol(){
        return this.symbol;
    }
    void setSymbol (char c) {
        setText("");
        setText(""+c);
        symbol = c;
    }
}
