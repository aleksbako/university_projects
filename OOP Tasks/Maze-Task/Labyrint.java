import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class Labyrint extends JPanel{
    private Rute[][] routes; // Contains all objects within the maze.
    private ArrayList<ArrayList<Tuppel>> paths; // the paths to exits within the maze.
    private MazeLayout mazeLayout;
    private ChoicePanel choicePanel;
    /**
     *
     * @param file - the file of the maze setup.
     */
    public Labyrint(File file){
        try {
            paths = new ArrayList<>();
            Scanner data = new Scanner(file);
            //Read first line to declare routes size.
            String sizes = data.nextLine();
            String[] s = sizes.split(" ");
            routes = new Rute[Integer.parseInt(s[0])][Integer.parseInt(s[1])];
            int y = 0;
            int x = 0;
            //go through the rest of the lines and add Route object into array.
            while(data.hasNext()){
                x=0;
                String route = data.next();

                for(int i = 0; i<route.length();i++) {
                    if (route.charAt(i) == '#') {
                        SortRute wall = new SortRute(y, x, this);
                            if(y != 0){
                                wall.setNorthRoute(routes[y-1][x]);
                                routes[y-1][x].setSouthRoute(wall);
                            }
                            if(x != 0){
                                wall.setWestRoute(routes[y][x-1]);
                                routes[y][x-1].setEastRoute(wall);
                            }
                        routes[y][x] = wall;
                        x++;

                    } else if (route.charAt(i) == '.') {
                        Rute path = null;
                        if (y == 0 || y == routes.length - 1 || x == 0 || x == routes[y].length - 1) {
                           path = new Aapning(y, x, this);
                        } else {
                            path = new HvitRute(y, x, this);
                        }
                        if(y != 0){
                           path.setNorthRoute(routes[y-1][x]);
                            routes[y-1][x].setSouthRoute(path);
                        }
                        if(x != 0){
                            path.setWestRoute(routes[y][x-1]);
                            routes[y][x-1].setEastRoute(path);
                        }
                        routes[y][x] = path;
                       x++;
                    }
                }
                y++;

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }



    }

    /**
     *
     * @param kol - column coordiantes of our starting point.
     * @param rad - row coordiantes of our starting point
     * @return - the list of all paths.
     */
    public ArrayList<ArrayList<Tuppel>> finnUtveiFra(int kol, int rad){
        //To avoid edge case that we try to access array outside of it's range.
        if((rad < routes.length && rad > 0) && (kol < routes[0].length && kol > 0)) {
            routes[rad][kol].finnUtvei();
            ArrayList<ArrayList<Tuppel>> temp = paths;
            paths = new ArrayList<>();
            return temp;
        }
        //Prints out if coordinates refer to the character starting outside of the maze.
        else{
            System.out.println("Coordiantes outside of the maze.");
        }
        //Default return if the coordinates are wrong.
        return new ArrayList<>();
    }

    public Rute[][] getRoutes() {
        return routes;
    }

    /**
     *
     * @param path - adds a path to the pathlist.
     */
    public void addPath(ArrayList<Tuppel> path){
        paths.add(path);
    }

    @Override
    public String toString() {
        String maze = "";
        for(int i = 0; i< routes.length;i++){
            String line = "";
            for(int x = 0; x<routes[i].length;x++){
                line += routes[i][x].tilTegn();
            }
            maze += line + "\n";
        }
        return maze;
    }

    /**
     * Initializes the labyrith with all it's routes as buttons.
     */
    public void initLabyrint(){
        setLayout(new GridLayout(routes.length,routes[0].length));
        for(int i = 0; i < routes.length;i++){
            for(int j = 0; j<routes[i].length;j++){
                routes[i][j].initRoute();
                add(routes[i][j]);
            }
        }

    }

    /**
     * Sets the current path each route at a time.
     * @param tupple - the touple of the a coordinate which will be marked as a path.
     */
    public void setPath(Tuppel tupple){

        for(int i = 0; i < routes.length;i++) {
            for (int j = 0; j < routes[i].length; j++) {
                if(routes[i][j].column == tupple.getColumn() && routes[i][j].row == tupple.getRow()){
                    routes[i][j].setSymbol('X');
                }
            }
        }

    }

    /**
     * Updates the path on the gui, by calling set path with each tupple element.
     * @param path
     */
    public void updatePath(ArrayList<Tuppel> path){
        for(int i = 0; i< path.size();i++){
            setPath(path.get(i));
        }
    }

    /**
     * Hides the previous math on the GUI.
     */
    public void hidePreviousPath(){
        for(int i = 0; i < routes.length;i++) {
            for (int j = 0; j < routes[i].length; j++) {
                if(routes[i][j].getSymbol() == 'X')
                    routes[i][j].setSymbol(' ');
                }
            }
    }

    /**
     * Adds a choice panel to the maze for easy access.
     * @param choice - choice panel which contains a pathlist object.
     */
    public void addChoicePanelt(ChoicePanel choice){
        this.choicePanel = choice;
    }

    /**
     * Updates the GUI list of all paths.
     * @param paths - the new list of all paths from a given tupple.
     */
    public void updatePathlist(ArrayList<ArrayList<Tuppel>> paths){
        choicePanel.updatePathList(paths);
        choicePanel.updateNumberOfPaths(paths.size());
    }

}
