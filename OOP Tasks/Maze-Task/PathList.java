import javax.swing.*;
import java.util.ArrayList;

/**
 * Jlist of which displays all the paths in the GUI as a string.
 */
public class PathList extends JList<String> {

    PathList(){
        DefaultListModel def = new DefaultListModel();
        this.setModel(def);
    }
    ArrayList<ArrayList<Tuppel>> paths;

    /**
     *
     * @param pathlist - list of all possible paths form the given position.
     */
    public void updatePaths(ArrayList<ArrayList<Tuppel>> pathlist) {
        if (pathlist.size() != 0) {
            DefaultListModel newmodel = new DefaultListModel();
            paths = pathlist;

            for (int i = 0; i < pathlist.size(); i++) {
                newmodel.addElement(pathlist.get(i).toString());
            }
            this.setModel(newmodel);

        }

    }
}
