import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Aapning extends HvitRute{
    /**
     *
     * @param row - the row coordinates of the element in maze
     * @param colomn - the column coordinates of the element in maze
     * @param labyrint - the laberyth to which the object belongs to.
     */
    public Aapning(int row, int colomn,Labyrint labyrint){
        super(row,colomn,labyrint);
    }
    @Override
    public void gaa(ArrayList<Tuppel> path){
        path.add(new Tuppel(column,row));
        labyrint.addPath(path);
        visited = false;

    }
    /**
     * Initializes the route object and makes it clickable for the opening.
     */
    @Override
    void initRoute () {
        setBorder(BorderFactory.createLineBorder(Color.black));
      //  setBackground(Color.green);
        setFont(new Font("Monospaced", Font.BOLD, 50));
        setPreferredSize(new Dimension(80, 80));
        setText(" ");

        Rute denneRuten = this;

        class ChoosePath implements ActionListener {
            @Override
            public void actionPerformed (ActionEvent e) {
                ArrayList<ArrayList<Tuppel>> temp = labyrint.finnUtveiFra(denneRuten.column, denneRuten.row);
                labyrint.updatePathlist(temp);
                if (temp.size() != 0) {
                    for (int i = 0; i < temp.get(0).size(); i++) {
                        labyrint.setPath(temp.get(0).get(i));
                        System.out.println(temp.get(0).get(i).getColumn() + "," + temp.get(0).get(i).getRow());
                    }

                }
            }
        }
        addActionListener(new ChoosePath());

    }
}
