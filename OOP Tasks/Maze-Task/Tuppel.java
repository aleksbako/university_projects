public class Tuppel {
    private int row;
    private int column;

    /**
     *
     * @param x - column coordiantes of the maze.
     * @param y - row coordiantes of the maze.
     */
   public  Tuppel(int x, int y){
       this.column = x;
        this.row = y;

    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }
    @Override
    public String toString(){
       return "("+column + "," + row+")";
    }
}
