import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

/**
 * Maze application layout panel, contains The labyrint nad the choice panel.
 */
public class MazeLayout extends JPanel {
    Labyrint labyrint;
    ChoicePanel choice; // Option panel.

    /**
     *
     * @param labyrint - labyrinth object which contains the layout of the map file.
     * @param choice - the choice panel which contains exit button, and all the possible paths from a given location.
     */
    MazeLayout(Labyrint labyrint, ChoicePanel choice){
    this.labyrint = labyrint;

    this.choice = choice;
    labyrint.addChoicePanelt(choice);
    choice.addLabyrint(labyrint);
    }

    /**
     * Initializes the maze application layout panel.
     */
    public void initMazelayout(){
        setLayout(new GridLayout(1,2));
        labyrint.initLabyrint();
        choice.initChoice();
        add(choice);
       add(labyrint);

    }

}
