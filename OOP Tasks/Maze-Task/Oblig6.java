import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.ArrayList;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class Oblig6 {
    // 2021  Versjon 1.0

    public static void main(String[] args) {


        File fil = null;
        Labyrint l = null;
        try {
            //Lets the user find the text file to choose the map.
            JFileChooser velger =
                    new JFileChooser();
            int resultat =
                    velger.showOpenDialog(null);

            if (resultat == JFileChooser.APPROVE_OPTION)
            {

                fil =
                        velger.getSelectedFile();
            } else {
                // Cancel
            }
            //Opens GUI window
            JFrame vindu = new JFrame("Maze");
            vindu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);



            l = new Labyrint(fil);

            MazeLayout mazeLayout = new MazeLayout(l,new ChoicePanel());
            mazeLayout.initMazelayout();
            vindu.add(mazeLayout);
            vindu.pack();
            vindu.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.printf("FEIL: Kunne ikke lese fra '%s'\n");
            System.exit(1);
        }

        // les start-koordinater fra standard input
        Scanner inn = new Scanner(System.in);
        System.out.println("Skriv inn koordinater <kolonne> <rad> ('a' for aa avslutte)");
        String[] ord = inn.nextLine().split(" ");

        while (!ord[0].equals("a")) {

            try {
                int startKol = Integer.parseInt(ord[0]);
                int startRad = Integer.parseInt(ord[1]);
                System.out.println("Utveier:");
                ArrayList<Tuppel> shortestpath = null;
                ArrayList<ArrayList<Tuppel>> utveier = l.finnUtveiFra(startKol, startRad);
                int countways = 0;
                for (ArrayList<Tuppel> lis: utveier) {

                    for(Tuppel t: lis)
                        System.out.println(t.toString());
                    countways++;

                    System.out.println();
                    if(shortestpath == null){
                        shortestpath = lis;
                    }
                    else{
                        if(lis.size()<shortestpath.size()){
                            shortestpath = lis;
                        }
                    }
                }
                System.out.println("\nAmount of paths out: " + countways);
                System.out.println("\nShortest path to exit: ");
                if(shortestpath != null) {
                    for (Tuppel tup : shortestpath)
                        System.out.println(tup.toString());
                }

                System.out.println();
            } catch (NumberFormatException e) {
                System.out.println("Ugyldig input!");
            }
            System.out.println("Skriv inn nye koordinater ('a' for aa avslutte)");
            ord = inn.nextLine().split(" ");

        }



    }
}