import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Object which is a walkable path in the maze.
 */
public class HvitRute extends Rute{
    /**
     *
     * @param row - the row coordinates of the element in maze
     * @param column - the column coordinates of the element in maze
     * @param labyrint - the laberyth to which the object belongs to.
     */
    public HvitRute(int row, int column, Labyrint labyrint){
        super(row,column,labyrint);
    }
    @Override
    public String tilTegn(){
        return ".";
    }

    @Override
    public void gaa(ArrayList<Tuppel> path) {


       if(this.visited){
            return;
        }
        Tuppel temp = null;
        if(path.size() == 0){
            temp = new Tuppel(column,row);
        }
        else {
             temp = path.get(path.size() - 1);
        }
        currentPath = path;
        currentPath.add(new Tuppel(column,row));
        visited = true;
        if(temp.getColumn() != column-1){
            this.westRoute.gaa((ArrayList<Tuppel>) currentPath.clone());
        }
        if(temp.getColumn() != column+1){
            this.eastRoute.gaa((ArrayList<Tuppel>) currentPath.clone());
        }
        if(temp.getRow() != row-1){
            this.northRoute.gaa((ArrayList<Tuppel>) currentPath.clone());
        }
        if(temp.getRow() != row+1){
            this.southRoute.gaa((ArrayList<Tuppel>) currentPath.clone());
        }
        visited = false;


    }
    @Override
    /**
     * Initializes the route object and makes it clickable for the white rute.
     */
    void initRoute () {
        setBorder(BorderFactory.createLineBorder(Color.black));
        //setBackground(Color.white);
        setFont(new Font("Monospaced", Font.BOLD, 50));
        setPreferredSize(new Dimension(80, 80));
        setText(" ");

        Rute denneRuten = this;

        class ChoosePath implements ActionListener {
            @Override
            public void actionPerformed (ActionEvent e) {
                ArrayList<ArrayList<Tuppel>> temp = labyrint.finnUtveiFra(denneRuten.column, denneRuten.row);
                labyrint.hidePreviousPath();
                if (temp.size() != 0) {
                    labyrint.updatePathlist(temp);
                    for (int i = 0; i < temp.get(0).size(); i++) {

                        labyrint.setPath(temp.get(0).get(i));

                    }
                }
            }
        }
        addActionListener(new ChoosePath());

    }
}
