import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Panel which containss Path list, lobels and Exit application.
 */
public class ChoicePanel extends JPanel {
    PathList pathslist;
    Labyrint labyrint;
    Label amountOfPaths;
    public void initChoice(){
        setLayout(new GridLayout(2,1));

        JButton exitButton = new JButton("Exit");

        /**
         * Exit Application buttons Action Listener
         */
        class exitApplication implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        }

        exitButton.addActionListener(new exitApplication());

        JPanel jPanel1 = new JPanel();
        jPanel1.setLayout(new GridLayout(2,1));
        jPanel1.add(exitButton);

        JPanel Jpanel2 = new JPanel();
        Jpanel2.setLayout(new BoxLayout(Jpanel2,BoxLayout.LINE_AXIS));

        amountOfPaths =  new Label( "Amount of Paths out: ");

        Jpanel2.add(amountOfPaths);


        Label temp1 =  new Label("Paths from the given location Shown below: ");

        Jpanel2.add(temp1);

        jPanel1.add(Jpanel2);
        add(jPanel1);


         pathslist = new PathList();
         //Adds a listSelectionlistener for when the new path is chosen from the list.
        pathslist.addListSelectionListener(new ListSelectionListener() {
            @Override

            public void valueChanged(ListSelectionEvent e) {
                String temp = pathslist.getSelectedValue();
                labyrint.hidePreviousPath();
                //System.out.println(pathslist);
                if(temp != null) {
                    for (int i = 0; i < pathslist.paths.size(); i++) {
                        if (temp.equals(pathslist.paths.get(i).toString())) {
                            labyrint.updatePath(pathslist.paths.get(i));
                        }
                    }
                }
                else{
                    System.out.println("Please click again the path list wasn't updated.");
                }

            }
        });
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(pathslist);
        add(scrollPane);
    }

    /**
     *
     * @param paths
     */
    public void updatePathList(ArrayList<ArrayList<Tuppel>> paths){
        this.pathslist.updatePaths(paths);
    }

    /**
     *
     * @param labyrint
     */
    public void addLabyrint(Labyrint labyrint){
        this.labyrint = labyrint;
    }

    /**
     *
     * @param amount
     */
    public void updateNumberOfPaths(int amount){
        amountOfPaths.setText("Amount of Paths out: " + amount);
    }
}
