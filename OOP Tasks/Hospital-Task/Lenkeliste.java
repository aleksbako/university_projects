import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Declared linkedlist which implements Liste interface.
 * @param <T> - Generic data type of the list.
 */
public class Lenkeliste <T> implements Liste<T>{
    //The first node of the list.
    protected Node start = null;

    /**
     * Node object which contains some data.
     */
    class Node{
        Node neste = null;
        T data;
        Node (T x) {
            data = x;
        }
    }


    class LenkelisteIterator<T> implements Iterator<T> {
        Node current = start;

        /**
         * Checks if current node is null.
         * @return boolean based on current != null.
         */
        @Override
        public boolean hasNext() {
            return current != null;
        }

        /**
         * Returns the data of the next node and sets the next node as current.
         * @return data of next node.
         */
        @Override
        public T next() {
            if(hasNext()){
                T data = (T) current.data;
                current = current.neste;
                return data;
            }
            return null;
        }

        @Override
        public void remove() {

        }


        public void forEachRemaining(Consumer<? super T> action) {

        }
    }



    /**
     * Returns the size of the list.
     * @return size of list.
     */
    @Override
    public int stoerrelse() {
        Node temp = start;
        int count = 0;
        while(temp != null){
            count++;
            temp = temp.neste;
        }
        return count;
    }

    /**
     * Adds new node to the list in a given position.
     * @param pos - position of the newly added node.
     * @param x - data of the added node.
     */
    @Override
    public void leggTil(int pos, T x) {
        if(pos > this.stoerrelse() || pos < 0){
            throw new UgyldigListeIndeks(-1);
        }
        Node temp = start;
        if(pos == 0){
            Node oldnode = start;
            start = new Node(x);
            start.neste = oldnode;
        }

        for(int i = 0; i< pos;i++){

            if(i == pos-1){
                Node node = new Node(x);
                node.neste = temp.neste;
                temp.neste = node;
            }
            temp = temp.neste;

        }


    }

    /**
     * Adds new node to the list
     * @param x - data of a node which will be added.
     */
    @Override
    public void leggTil(T x) {
            Node newnode = new Node(x);
        if(start == null){
            start = newnode;
        }
        else {
            Node temp = start;
            while (temp.neste != null) {
                temp = temp.neste;
            }

            temp.neste = newnode;
        }

    }

    /**
     * Replaces a given nodes data in a position with new data.
     * @param pos - position where to place the node.
     * @param x - the data which will replace the data of the given node in a certain position.
     */
    @Override
    public void sett(int pos, T x) {
        if(pos > this.stoerrelse()-1 || (pos == 0 && start == null) || pos < 0){

            throw new UgyldigListeIndeks(-1);
        }

        if(pos == 0){
            Node oldnode = start;
            start = new Node(x);
            start.neste = oldnode.neste;
        }
            Node temp = start;
            for(int i = 0; i< pos;i++) {
                if (i == pos-1 ) {
                    Node node = new Node(x);
                    Node tempreplace = temp.neste;
                    node.neste = tempreplace.neste;
                    temp.neste = node;
                }
                temp = temp.neste;
            }


    }

    /**
     *Fetches the data of a node.
     * @param pos - the position of the node which contains the data that you want to fetch.
     * @return the data of a requested node.
     */
    @Override
    public T hent(int pos) {
        if(pos > this.stoerrelse()-1 || pos < 0 || this.stoerrelse() == 0|| pos < 0){
            throw new UgyldigListeIndeks(-1);
        }
        Node temp = start;

        for(int i = 0; i< pos;i++) {
            temp = temp.neste;
        }
        return (T) temp.data;
    }

    /**
     *
     * @param pos - position of the node that will be deleted.
     * @return the data of the deleted node.
     */
    @Override
    public T fjern(int pos) {

        if(pos > this.stoerrelse()-1 || pos < 0 || this.stoerrelse() == 0 || pos < 0){
            throw new UgyldigListeIndeks(-1);
        }
        Node temp = start;
        Node nestetemp = null;
        if(pos == 0){
            Object data = start.data;
            start = start.neste;
            return (T) data;
        }
        for(int i = 0; i< pos;i++) {

            if (i == pos-1) {
                nestetemp = temp.neste;
                 temp.neste = nestetemp.neste;

            }
            temp = temp.neste;
        }
        return (T) nestetemp.data;

    }

    /**
     *Deletes the first node from the list.
     * @return the data of the deleted node.
     */
    @Override
    public T fjern() {
        if(start == null){
            throw new UgyldigListeIndeks(-1);
        }
      Node temp = start;
      start = temp.neste;
      return (T) temp.data;
    }

    /**
     * Returns string of all data.
     * @return a string of list data.
     */
    @Override
    public String toString(){
        Node node = start;
        String datastring = "";
        while(node != null){
            datastring += "Node: " + node.data + ",";
            node = node.neste;
        }
        return datastring;
    }

    /**
     * Returns a new lenkeliste Iterator (used for foreach loop iteration).
     * @return new LenkelisteIterator.
     */
    @Override
    public Iterator<T> iterator() {
        return new LenkelisteIterator<>();
    }


    public void forEach(Consumer<? super T> action) {

    }


    public Spliterator<T> spliterator() {
        return null;
    }


}
