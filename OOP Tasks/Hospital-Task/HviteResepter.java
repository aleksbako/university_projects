class HviteResepter extends Resepter{
  int pris;
  String farge = "Hvit";

  public HviteResepter(Legemidler legemiddel, Lege lege, Pasient pasient, int reit){
    super(legemiddel, lege, pasient, reit);
  }

  //siden alle hvite resepter er hvite skriver jeg hvit her.
  @Override
  public String farge(){
    return "hvit";
  }


  @Override
  public int prisAaBetale(){
    pris = legemiddel.hentPris();
    return pris;
  }

  @Override
  public String toString(){
    return "Resept::: "+ farge +"-Resept \n  " + legemiddel +" \n  "+
      lege +" \n  Pasient: " + pasient.PasientNavn() + " - Fodselsnummer: " + pasient.Fodselsnummer() + "\n  Reit: "+ reit +
      "\n  Pris å betale: " + prisAaBetale() + "kr";
  }
}
