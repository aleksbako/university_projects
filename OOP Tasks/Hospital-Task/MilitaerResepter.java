class MilitaerResepter extends HviteResepter{

  public MilitaerResepter(Legemidler legemiddel, Lege lege, Pasient pasient, int reit){
    super(legemiddel, lege, pasient, reit);
    int pris = 0;
  }

  @Override
  public int prisAaBetale(){
    pris = legemiddel.hentPris();
    pris = pris * 0;
    return pris;
  }

  @Override
  public String toString(){
    return "Resept::: "+ farge +"-Militaer resept \n  " + legemiddel +" \n  "+
      lege +" \n  Pasient: " + pasient.PasientNavn() + " - Fodselsnummer: " + pasient.Fodselsnummer() +"\n  Reit: "+ reit +
      "\n  Pris å betale: " + prisAaBetale() + "kr";
  }
}
