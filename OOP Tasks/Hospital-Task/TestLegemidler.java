class TestLegemidler{
  public static void main(String[] args) {

    // oppretter nytt narkotisk legemiddel
    Narkotisk nakotiskLegemiddel = new Narkotisk("Valium", 1000, 0.2, 1000);
    System.out.println(nakotiskLegemiddel.toString());

    //oppretter nytt vanedannende legemiddel
    Vanedannende vanedannendeLegemiddel = new Vanedannende("Morfin", 90, 0.5, 500);
    System.out.println(vanedannendeLegemiddel.toString());

    //oppretter vanlig legemiddel
    VanligeLegemidler vanligLegemiddel = new VanligeLegemidler("Kepra", 500, 2);
    System.out.println(vanligLegemiddel.toString());
  }
}

/*
OBS: jeg kan ingenting om legemidler, og vet derfor lite om virkestoff o.l. så aner ikke om dette er
noe riktig:)
*/
