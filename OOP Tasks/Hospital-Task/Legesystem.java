import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.PrintWriter;

/**
 * Legesystemet, HovedProgram.
 */
class Legesystem {
   static SortertLenkeliste<Lege> legeliste = new SortertLenkeliste<>();
   static Lenkeliste<Pasient> pasientlise = new Lenkeliste<>();
   static Lenkeliste<Legemidler> legemidler = new Lenkeliste<>();

  public static void main(String[] args) {
    LesFraFil();
    Meny();
  }

  public Legesystem() {

  }

  /**
   * LesFraFil - leser informasjon fra fil og setter objekter inn i lister.
   */
  public static void LesFraFil() {

    Scanner fil = null;

    try {
      System.out.println("Skriv inn filnavn: ");
      Scanner input = new Scanner(System.in);
      String filnavn = input.nextLine();
      fil = new Scanner(new File(filnavn));
    }
    catch (Exception e) {
      System.out.println("::::Feilmelding::::");
      System.exit(1);
    }


    String currentGroup = "";//Brukes til å identifisere hva slags gruppe vi skriver nå.
    while (fil.hasNextLine()) {
      String linje = "";
      linje = fil.nextLine();

      String[] delt = linje.split(" ");

      String[] komma = linje.split(",");

      if (delt[0].equals("#")) {
        if (delt[1].equals("Pasienter")) {
          currentGroup = "Pasienter";
          System.out.println("–––––––––––––Pasientliste–––––––––––––");
        }
        else if (delt[1].equals("Legemidler")) {
          currentGroup = "Legemidler";
          System.out.println();
          System.out.println("–––––––––––––Legemidler–––––––––––––");
        }
        else if (delt[1].equals("Leger")) {
          currentGroup = "Leger";
          System.out.println();
          System.out.println("–––––––––––––Leger–––––––––––––");
        }
        else if (delt[1].equals("Resepter")) {
          currentGroup = "Resepter";
          System.out.println();
          System.out.println("–––––––––––––Resepter–––––––––––––");
        }
      }

      else if(currentGroup.equals("Pasienter")){
        Pasient pasient = new Pasient(komma[0],komma[1]);
        pasientlise.leggTil(pasient);
      }

      else if(currentGroup.equals("Leger")){
        if(komma[1].equals("0")){
          Lege lege = new Lege(komma[0]);
          legeliste.leggTil(lege);
        }
        else{
          Spesialist specialist = new Spesialist(komma[0],komma[1]);
          legeliste.leggTil(specialist);
        }

      }

      else if(currentGroup.equals("Legemidler")){

       if (komma[1].equals("narkotisk")) {
          String[] and = linje.split(",");
          String navn = and[0];
          Double prisx = Double.parseDouble(and[2]);
          int pris = (int) Math.round(prisx);
          Double virkestoff = Double.parseDouble(and[3]);
          int styrke = Integer.parseInt(and[4]);
          Narkotisk n = new Narkotisk(navn, pris, virkestoff, styrke);
          legemidler.leggTil(n);

        }


        else if (komma[1].equals("vanedannende")) {
          String[] and = linje.split(",");
          String navn = and[0];
          Double prisx = Double.parseDouble(and[2]);
          int pris = (int) Math.round(prisx);
          Double virkestoff = Double.parseDouble(and[3]);
          int styrke = Integer.parseInt(and[4]);
          Vanedannende n = new Vanedannende(navn, pris, virkestoff, styrke);
          legemidler.leggTil(n);

        }
        // det blir en feil når jeg prover å opprette nytt objekt
        else if (komma[1].equals("vanlig")) {
          String[] and = linje.split(",");
          String navn = and[0];
          Double prisx = Double.parseDouble(and[2]);
          int pris = (int) Math.round(prisx);
          Double virkestoff = Double.parseDouble(and[3]);
          VanligeLegemidler n = new VanligeLegemidler(navn, pris, virkestoff);
          legemidler.leggTil(n);

        }
      }
      else if (currentGroup.equals("Resepter")){
          Lege templege = null;
          for(Lege lege : legeliste){
            if(lege.hentNavn().equals(komma[1])){
              templege = lege;
            }
          }

          Pasient tempPasient = null;
          for(Pasient pasient : pasientlise){
            if(pasient.hentPasientID() == Integer.parseInt(komma[2])){
              tempPasient = pasient;

            }
          }
          Legemidler templegemiddel = null;
          for(Legemidler l : legemidler){
            int tempID = Integer.parseInt(komma[0]);

            if(l.hentID() == tempID){
              templegemiddel = l;

            }
          }

          if(templegemiddel  != null && templege != null && tempPasient != null) {

            if (komma[3].equals("hvit")) {
              int reit = Integer.parseInt(komma[4]);
              try {
                HviteResepter hviteResepter = templege.skrivHvitResept(templegemiddel, tempPasient, reit);
                tempPasient.leggTilResept(hviteResepter);
              } catch (UlovligUtskrift e) {
                System.out.println("UlovligUtskrift for hvitresept");
              }


            } else if (komma[3].equals("blaa")) {
              int reit = Integer.parseInt(komma[4]);
              try {
                BlaResepter blaResepter = templege.skrivBlaaResept(templegemiddel, tempPasient, reit);
                tempPasient.leggTilResept(blaResepter);
              } catch (UlovligUtskrift e) {
                System.out.println("UlovligUtskrift for blaaresept");
              }


            } else if (komma[3].equals("millitaer")) {
              int reit = Integer.parseInt(komma[4]);
              try {
                BlaResepter blaResepter = templege.skrivBlaaResept(templegemiddel, tempPasient, reit);
                System.out.println(tempPasient.PasientNavn());
                tempPasient.leggTilResept(blaResepter);
              } catch (UlovligUtskrift e) {
                System.out.println("UlovligUtskrift for blaaresept");
              }


            } else if (komma[3].equals("p")) {
              try {
                PResepter pResepter = templege.skrivPResept(templegemiddel, tempPasient);
                tempPasient.leggTilResept(pResepter);
              } catch (UlovligUtskrift e) {
                System.out.println("UlovligUtskrift for presept");
              }


            }
          }

      }

    }
    Meny();
  }

  /**
   * Meny - Legesystemets navigasjons funksjon.
   */
  public static void Meny() {
    System.out.println("––––––MENY–––––––––––––––––––––––");
    System.out.println("1: Skriv ut fullstendig oversikt");
    System.out.println("2: Legg til nytt element");
    System.out.println("3: Bruke resept");
    System.out.println("4: Statistikk");
    System.out.println("5: Skriv data til fil");
    System.out.println("6: Avslutt");
    System.out.println("–––––––––––––––––––––––––––––––––");

    System.out.println("Skriv inn tall: ");
    Scanner input = new Scanner(System.in);
    int MenyValg = input.nextInt();
    System.out.println();
    try {
      if (MenyValg == 1) {
        System.out.println("–––Skriver ut fullstendig oversikt–––");
        Oversikt();
      }

      else if (MenyValg == 2) {
        Leggtil();
      }

      else if (MenyValg == 3) {
        System.out.println("–––Bruke resept–––");
        BrukResept();
      }

      else if (MenyValg == 4) {
        Statistikk();
      }

      else if (MenyValg == 5) {
        System.out.println("–––Skrive data til fil–––");
        SkrivTilFil();
      }

      else if (MenyValg == 6) {
        System.out.println("..............Avslutter................");
        System.exit(0);
      }

      else {
        System.out.println(":::::FEILMELDING:::::");
        Meny();
      }
    }
    catch(Exception e) {
      e.printStackTrace();
      System.out.println(":::::FEILMELDING:::::");
      Meny();
    }
  }

  /**
   * Oversikt - skriver ut en oversikt over hva slags elementer vi har i systemet nå.
   */
  public static void Oversikt() {
    System.out.println("Lege Liste: \n");
    for(Lege lege : legeliste){
        System.out.println(lege.toString());
        System.out.println("UtskrevendeResept liste: \n");
        for(Resepter r : lege.hentUtsekrevendeResepter()){
          System.out.println(r.toString());
        }
    }
    System.out.println("Pasient Liste: \n");
    for(Pasient pasient: pasientlise){
      System.out.println(pasient.toString());
    }
    System.out.println("Legemidler Liste: \n");
    for(Legemidler legemidler : legemidler){
      System.out.println(legemidler.toString());
    }
    Meny();

  }

  /**
   * LegeTil - Meny der du kan velge å legge til et element til systemet. Kaller hjelpe funksjoner for å legge til den valgte element.
   */
  public static void Leggtil() {
    System.out.println("––––––MENY Legg til element–––––––––––––––––––––––");
    System.out.println("1: Legg til lege");
    System.out.println("2: Legg til pasient");
    System.out.println("3: Legg til resept");
    System.out.println("4: Legg til legemiddel");
    System.out.println("5: Tilbake til hovedmeny");
    System.out.println("6: Avslutt");
    System.out.println("–––––––––––––––––––––––––––––––––");


    Scanner input = new Scanner(System.in);
    int MenyValg = input.nextInt();
    System.out.println();
    try {
      if (MenyValg == 1) {
        System.out.println("–––Legg til lege–––");
        LeggtilLege();
      }
      else if (MenyValg == 2) {
        System.out.println("–––Legg til pasient–––");
        LeggtilPasient();
      }

      else if (MenyValg == 3) {
        LeggTilResept();
      }

      else if (MenyValg == 4) {
        LeggtilLegemiddel();
      }

      else if (MenyValg == 5) {
        Meny();
      }

      else if (MenyValg == 6) {
        System.out.println("..............Avslutter................");
        System.exit(0);
      }

      else {
        System.out.println(":::::FEILMELDING:::::");
        Leggtil();
      }
    }
    catch(Exception e) {
      System.out.println(":::::FEILMELDING:::::");
      Leggtil();
    }

    Meny();
  }

  /**
   * LeggtilLege - Hjelper funskjon for å legge til lege til systemet.
   */
  public static void LeggtilLege() {
    Scanner input = new Scanner(System.in);
    System.out.println("Skriv inn navn på lege: ");
    String lege = "Dr. " + input.nextLine();

    System.out.println("Er " + lege + " en spesialist? (ja/nei)");
    String svar = input.next();
    if (svar.equals("nei")) {
      Lege nylege = new Lege(lege);
      System.out.println("lege lagt til");
      legeliste.leggTil(nylege);
      //legg til lege i legeliste
    }
    else if (svar.equals("ja")) {
      System.out.println("Skriv inn kontrollID: ");
      String kontrollid = input.next();
      Spesialist nyspesialist = new Spesialist(lege,kontrollid);
      System.out.println("spesialist lagt til");
      legeliste.leggTil(nyspesialist);
      //legg til lege i legeliste
    }

    Meny();
  }

  /**
   * LeggtilPasient - Hjelper funskjon for å legge til Pasient til systemet.
   */
  public static void LeggtilPasient() {
    Scanner input = new Scanner(System.in);
    System.out.println("Skriv inn navn på pasient: ");
    String navn = input.nextLine();

    System.out.println("Skriv inn fodselsnummer til pasient: ");
    String fodselsnr = input.next();

    Pasient nypasient = new Pasient(navn, fodselsnr);
    System.out.println("Pasient lagt til! - " + navn + " : " + fodselsnr);
    pasientlise.leggTil(nypasient);
    //legg til pasient i pasient liste

    Meny();
  }

  /**
   * LeggTilResept - Hjelper funskjon for å legge til Resept til systemet.
   * OBS: Må ha lege, legemiddel og pasienten i systemet før bruk.
   */
  public static void LeggTilResept() {
    Scanner input = new Scanner(System.in);
    System.out.println("Navn på pasient: ");
    String pasient = input.nextLine();
    Pasient tempPasient = null;
    //Om lege er i liste saa sett templege til lege
    for(Pasient p: pasientlise){
      if(p.PasientNavn().equals(pasient)){
        tempPasient = p;
        break;
      }
    }
    if(tempPasient == null) {
      System.out.println("Pasient finnes ikke i systemet! Sjekk om du har skrevet navnet riktig, ellers legg den til systemet.");
      Meny();
    }



    System.out.println("Utskrivende lege: ");
    String lege = input.nextLine();
    Lege tempLege = null;
    //Om lege er i liste saa sett templege til lege
    for(Lege l : legeliste){
      if(l.hentNavn().equals((lege))){
        tempLege = l;
        break;
      }
      //Om noen ikke skriver Dr. men lege finnes.
      else if(l.hentNavn().equals(("Dr. " + lege))){
        tempLege = l;
        break;
      }
    }
    if(tempLege == null) {
      System.out.println("Lege finnes ikke i systemet! Sjekk om du har skrevet navnet riktig, ellers legg den til systemet.");
      Meny();
    }


    System.out.println("Legemiddel: ");
    String legemiddelNavn = input.nextLine();
    Legemidler tempLegemiddel = null;
    for(Legemidler legemidel: legemidler){
      if(legemidel.hentNavn().equals(legemiddelNavn)){
        tempLegemiddel = legemidel;
      }
    }
    if(tempLegemiddel == null) {
      System.out.println("Legemiddel finnes ikke i systemet! Sjekk om du har skrevet navnet riktig, ellers legg den til systemet.");
        Meny();
    }

    System.out.println("Hvor mange ganger kan resepten brukes: ");
    int reit = input.nextInt();

    System.out.println("–––Type resept–––");
    System.out.println("1: Blaa resept");
    System.out.println("2: Hvit resept");
    System.out.println("3: P resept");
    System.out.println("4: Militaer resept");
    int MenyValg = input.nextInt();

    try {
      if (MenyValg == 1) {
        System.out.println("–––Blaa resept–––");
       BlaResepter nyBlaresept = tempLege.skrivBlaaResept(tempLegemiddel, tempPasient, reit);
        tempPasient.leggTilResept(nyBlaresept);
      }
      else if (MenyValg == 2) {
        System.out.println("–––Hvit resept–––");
        HviteResepter nyHviteResept = tempLege.skrivHvitResept(tempLegemiddel, tempPasient, reit);
        tempPasient.leggTilResept(nyHviteResept);
      }

      else if (MenyValg == 3) {
        System.out.println("–––P resept–––");
        PResepter nyPresept = tempLege.skrivPResept(tempLegemiddel,tempPasient);
        tempPasient.leggTilResept(nyPresept);
      }

      else if (MenyValg == 4) {
        System.out.println("–––Militaer resept–––");
        MilitaerResepter nymilitaerResepter = tempLege.skrivMillitaerResept(tempLegemiddel,tempPasient,reit);
        tempPasient.leggTilResept(nymilitaerResepter);

      }

      else {
        System.out.println(":::::FEILMELDING:::::");
        LeggTilResept();
      }

    }
    catch(Exception e) {
      System.out.println(":::::FEILMELDING:::::");
      LeggTilResept();
    }

    Meny();
  }

  /**
   * LeggtilLegemiddel - Hjelper funskjon for å legge til Legemiddel til systemet.
   */
  public static void LeggtilLegemiddel() {
    Scanner input = new Scanner(System.in);
    System.out.println("–––Type legemiddel–––");
    System.out.println("1: Narkotisk");
    System.out.println("2: Vanedannende");
    System.out.println("3: Vanlig");
    int type = input.nextInt();
    try{
      if (type == 1) {
        System.out.println("…………Narkotisk…………");
        System.out.println("Navn på legemiddel: ");
        input.nextLine();
        String navn = input.nextLine();
        System.out.println("Pris: ");
        int pris = input.nextInt();
        System.out.println("Virkestoff: ");
        double virkestoff = input.nextDouble();
        System.out.println("Narkotisk styrke:");
        int styrke = input.nextInt();
        Narkotisk n = new Narkotisk(navn,pris,virkestoff,styrke);
        legemidler.leggTil(n);

      }
      else if (type == 2) {
        System.out.println("…………Vanedannende…………");
        System.out.println("Navn på legemiddel: ");
        input.nextLine();
        String navn = input.nextLine();
        System.out.println("Pris: ");
        int pris = input.nextInt();
        System.out.println("Virkestoff: ");
        double virkestoff = input.nextDouble();
        System.out.println("Vanedannende styrke:");
        int styrke = input.nextInt();
        Vanedannende v = new Vanedannende(navn,pris,virkestoff,styrke);
        legemidler.leggTil(v);

      }
      else if (type == 3) {
        System.out.println("…………Vanlig…………");
        System.out.println("Navn på legemiddel: ");
        input.nextLine();
        String navn = input.nextLine();
        System.out.println("Pris: ");
        int pris = input.nextInt();
        System.out.println("Virkestoff: ");
        double virkestoff = input.nextDouble();
        VanligeLegemidler v = new VanligeLegemidler(navn,pris,virkestoff);
        legemidler.leggTil(v);

      }
      else {
        System.out.println(":::::FEILMELDING:::::");
        LeggtilLegemiddel();
      }
    }
    catch(Exception e) {
      System.out.println(":::::FEILMELDING:::::");
      LeggtilLegemiddel();
    }

    Meny();
  }

  /**
   * BrukResept - Bruker reseptet å senker reit med 1 om det er mulig å bruke den, ellers bare skriv ut at det er ikke mulig å bruke reseptet.
   */
  public static void BrukResept() {
    Scanner input = new Scanner(System.in);
    System.out.println("Hvilken pasient vil du se resepter for? ");
    int idteller = 0;
    for(Pasient p: pasientlise){
      System.out.println(idteller + ": " + p.PasientNavn() + "(fnr " + p.Fodselsnummer() + ")");
      idteller++;
    }

    //evt. skrive ut en liste med alle pasienter
    int valgtPasient = input.nextInt();
    Pasient pasient = pasientlise.hent(valgtPasient);
    Stabel<Resepter> resepter = pasient.hentResepter();
    System.out.println("Valgt pasient: " + valgtPasient);

    System.out.println("Hvilken resept vil du bruke? ");
    idteller = 0;
    for(Resepter resept : resepter){
      System.out.println(idteller+ ": "+resept.hentLegemiddel().hentNavn() + "(" + resept.hentReit() + ")");
      idteller++;

    }
    //oversikt over pasientens Resepter
    int valgtResept = input.nextInt();
    System.out.println(valgtResept);
    Resepter resept = resepter.hent(valgtResept);


    if(resept.bruk()) {
      System.out.println("Brukte resept på " + valgtResept + ". Antall gjenvaerende reit: " + resept.hentReit());

    }
    //hvis det ikke er flere reit
    else {
      System.out.println("Kunne ikke bruke resept paa " + valgtResept + " (Ingen gjenvaerende reit)");
    }
    Meny();
  }

  /**
   * Statistikk - Skriver ut stistikk om hva slags resepter ble skrevet ut, hvor mange.
   */
  public static void Statistikk() {
    System.out.println("––––––MENY Statistikk–––––––––––––––––––––––");
    System.out.println("1: Skriv ut fullstendig Statistikk");
    System.out.println("2: Totalt antall utskrevne resepter på vanedannende legemidler");
    System.out.println("3: Totalt antall utskrevne resepter på narkotiske legemidler");
    System.out.println("4: Statistikk om mulig misbruk av narkotiske legemidler");
    System.out.println("5: Tilbake til hovedmeny");
    System.out.println("6: Avslutt");
    System.out.println("–––––––––––––––––––––––––––––––––");

    System.out.println("Skriv inn tall: ");
    Scanner input = new Scanner(System.in);
    int MenyValg = input.nextInt();
    System.out.println();
    try {
      if (MenyValg == 1) {
        System.out.println("–––Skriver ut fullstendig Statistikk–––");
        int Vanligcount = 0;
        int Narkotiskcount = 0;
        int Vanedannedecount = 0;
        for(Lege lege : legeliste){
          for(Resepter resepter : lege.hentUtsekrevendeResepter()){
            if(resepter.hentLegemiddel() instanceof  Vanedannende){
              Vanedannedecount++;
            }
            else if(resepter.hentLegemiddel() instanceof  Narkotisk){
              Narkotiskcount++;
            }
            else{
              Vanligcount++;
            }
          }
        }
        System.out.println("Antall resepter med vanlige legemiddler: " + Vanligcount);
        System.out.println("Antall resepter med Vanedannede legemiddler: " + Vanedannedecount);
        System.out.println("Antall resepter med Narkotiske legemiddler: " + Narkotiskcount);
      }
      else if (MenyValg == 2) {
        System.out.println("–––Totalt antall utskrevne resepter på vanedannende legemidler–––");
        int count = 0;
        for(Lege lege : legeliste){
          for(Resepter resepter : lege.hentUtsekrevendeResepter()){
            if(resepter.hentLegemiddel() instanceof  Vanedannende){
              count++;
            }
          }
        }
        System.out.println("Det er " + count + " resepter som har vanedannende legemidler.");
      }

      else if (MenyValg == 3) {
        System.out.println("–––Totalt antall utskrevne resepter på narkotiske legemidler–––");
        int count = 0;
        for(Lege lege : legeliste){
          for(Resepter resepter : lege.hentUtsekrevendeResepter()){
            if(resepter.hentLegemiddel() instanceof  Narkotisk){
              count++;
            }
          }
        }
        System.out.println("Det er " + count + " resepter som har narkotiske legemidler.");
      }

      else if (MenyValg == 4) {
        System.out.println("–––Statistikk om mulig misbruk av narkotiske legemidler–––");
        for(Lege lege : legeliste){
          int count = 0;
          for(Resepter resepter : lege.hentUtsekrevendeResepter()){
            if(resepter.hentLegemiddel() instanceof  Narkotisk){
              count++;
            }
          }
          System.out.println("Lege Navn: " + lege.hentNavn() + ", Antall resepter for narkotiske legemiddler: " + count);
        }
        for(Pasient pasient : pasientlise){
          int count = 0;
          Stabel<Resepter> resepterlise = pasient.hentResepter();
          for(Resepter resepter : resepterlise){
            if(resepter.hentLegemiddel() instanceof  Narkotisk){
              count++;
            }
          }
          System.out.println("Pasient Navn: " + pasient.PasientNavn() + ", Antall resepter for narkotiske legemiddler: " + count);
        }
      }

      else if (MenyValg == 5) {
        System.out.println("5: Tilbake til hovedmeny");
        Meny();
      }

      else if (MenyValg == 6) {
        System.out.println("..............Avslutter................");
        System.exit(0);
      }

      else {
        System.out.println(":::::FEILMELDING:::::");
        Statistikk();
      }
    }
    catch(Exception e) {
      System.out.println(":::::FEILMELDING:::::");
      Statistikk();
    }
  Meny();
  }


  /**
   * SkrivTilFil - Skriver alt data som er i systemet til et fil.
   */
  public static void SkrivTilFil() {
    PrintWriter f = null;
    Scanner input = new Scanner(System.in);
    String outputNavn = input.next();
    try{
      f = new PrintWriter (outputNavn);
    }catch(Exception e){
      System.out.println("Kan ikke lage filen");
      System.exit(1);
    }
    f.print("# Pasienter (navn, fnr)\n");
    for (Pasient pasient : pasientlise){
      f.print(pasient.PasientNavn()+","+pasient.Fodselsnummer()+"\n");
    }
    f.print("# Legemidler (navn, type, pris, virkestoff, [styrke])\n");
    for (Legemidler l : legemidler){
      if (l instanceof VanligeLegemidler){
        f.print(l.hentNavn()+",vanlig,"+l.hentPris()+","+(int)Math.round(l.hentVirkestoff())+"\n");
      }
      else if (l instanceof Narkotisk){
        // får feilmelding på denne (hentNarkotiskStyrke), ikke alle Legemidler har styrke
        Narkotisk nark = (Narkotisk) l;
        f.print(nark.hentNavn()+",narkotisk,"+nark.hentPris()+","+(int)Math.round(nark.hentVirkestoff()) +"," +nark.hentNarkotiskStyrke()+"\n");
      }
      else{
        Vanedannende vanedannende = (Vanedannende) l;
        f.print(vanedannende.hentNavn()+",vanedannende,"+vanedannende.hentPris()+","+(int)Math.round(vanedannende.hentVirkestoff())+ ","+ vanedannende.hentVanedannendeStyrke() + "\n");
      }
    }
    f.print("# Leger (navn, kontrollID/0 hvis vanlig lege)\n");
      for (Lege lege : legeliste){
        //får feilmelding når jeg prøver å skrive enten spesialist eller ikke spesialist; tror det er if-testen som feiler
        if (lege instanceof Spesialist){
          Spesialist specialist = (Spesialist) lege;
          f.print(specialist.hentNavn()+ "," + specialist.kontrollID +",\n");
        }
        else{
          f.print(lege.hentNavn()+"," + 0+"\n");
        }
    }
    f.print("# Resepter (legemiddelNummer, legeNavn, pasientID, type, [reit])\n");
      for  (Lege lege2 : legeliste){ // Har ikke fått testet denne ordentlig pga feilmelding med lege
        for(Resepter resept : lege2.hentUtsekrevendeResepter()){
          String type = "";
          if(resept instanceof BlaResepter){
            type = "blaa";
          }
          else if(resept instanceof MilitaerResepter){
            type = "millitaer";
          }
          else if(resept instanceof PResepter){
            type = "p";
          }
          else if(resept instanceof HviteResepter){
            type = "hvit";
          }
          f.print(resept.hentLegemiddel().hentID()+","+lege2.hentNavn()+","+ resept.hentPasientID()+","+type+","+resept.hentReit()+"\n");
        }
      }

    f.println();
    f.close();


    Meny();
  }
}
