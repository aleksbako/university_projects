abstract class Resepter{
  protected int reseptID;
  protected Legemidler legemiddel;//referanse til et legemiddel
  protected Lege lege;//referanse til legen
  protected Pasient pasient; //ID til pasienten som eier resepten
  protected int reit;
  protected static int teller = 1; // skal holde styr på reseptID.
  protected int pris;

  public Resepter(Legemidler legemiddel, Lege lege, Pasient pasient, int reit){
    this.reseptID = reseptID;
    this.legemiddel = legemiddel;
    this.lege = lege;
    this.pasient = pasient;
    this.reit = reit;
    reseptID = teller; // setter IDen til å være lik 1
    teller ++;
  }

  // oppretter metode for å hente IDen
  public int hentID(){
    return reseptID;
  }

  // oppretter metode for å hente legemiddel
  public Legemidler hentLegemiddel(){
    return legemiddel;
  }

  // oppretter metode for å hente lege
  public Lege hentLege(){
    return lege;
  }

  // oppretter metde for å hente pasientID
  public int hentPasientID(){
    return pasient.hentPasientID();
  }

  //oppretter metode for å hente antall reit igjen
  public int hentReit(){
    return reit;
  }

  //oppretter metode for å sjekke om man fremdeles kan bruke resepten
  public boolean bruk(){
    if (reit > 0){
      this.reit --;
      return true;
    }
    else
    {
      return false;
    }
  }

  //oppretter abstrakt metode farge for blå og hvite resepter
  abstract public String farge();

  // oppretter bstrakt metode pris å betale for å sjekke prisen å betale i subklassene.
  abstract public int prisAaBetale();


  // overskriver toString metoden til Object-klassen for å enkelt skrive ut informasjon om resept.
  public String toString() {
    return legemiddel.toString() +" - "+ lege +" - "+ pasient.PasientNavn() + " - "+ reit;
  }
}
