
public class Pasient{
  private String navn;
  private String fodselsnr;
  protected int pasientID;
  private static int teller = 1;
  private Stabel<Resepter> reseptListe = new Stabel<Resepter>();

  public Pasient(String navn, String fodselsnr){
    this.navn = navn;
    this.fodselsnr = fodselsnr;

    pasientID = teller;
    teller ++;
  }

  public void leggTilResept(Resepter resept){
    reseptListe.leggPaa(resept);
  }

  public Stabel hentResepter(){
    return reseptListe;
  }

  public String PasientNavn() {
    return navn;
  }

  public String Fodselsnummer() {
    return fodselsnr;
  }

  public int hentPasientID(){
    return pasientID;
  }

  @Override
  public String toString(){
    return "Resepter til " + navn + " er: " + "\n" + reseptListe;
  }
}
