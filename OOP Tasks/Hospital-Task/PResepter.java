class PResepter extends HviteResepter{
  int pris;

  public PResepter(Legemidler legemiddel, Lege lege, Pasient pasient){
    super(legemiddel, lege, pasient, 3); //3 er antall reit i P-resepter.
  }

  @Override
  public int prisAaBetale(){
    pris = legemiddel.hentPris();
    if (pris < 108){
      pris = 0;
    }
    else{
      pris = pris - 108;
    }
    return pris;
  }

  @Override
  public String toString(){
    return "Resept::: "+ farge +"-PResept \n  " + legemiddel +" \n  "+
      lege +" \n  Pasient: " + pasient.PasientNavn() + " - Fodselsnummer: " + pasient.Fodselsnummer() + "\n  Reit: "+ reit +
      "\n  Pris å betale: " + prisAaBetale() + "kr";
  }
}
