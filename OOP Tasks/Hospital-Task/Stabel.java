/**
 * Implementation of a stack through a linked list.
 * @param <T> - generric data
 */
public class Stabel <T> extends Lenkeliste<T>{
    /**
     * Push function, puts the element at the end of the list
     * @param x - the data of a node that will be added to the end of the list
     */
    public void leggPaa(T x){
        this.leggTil(x);
    }

    /**
     * Pops the element from the end of the list.
     * @return the data of a node.
     */
    public T taAv(){
        int len = this.stoerrelse();
        T x = fjern(len-1);
    return x;
    }
}
