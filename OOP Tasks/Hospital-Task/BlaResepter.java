
class BlaResepter extends Resepter{
  int pris;
  String farge = "Blaa";

  public BlaResepter(Legemidler legemiddel, Lege lege, Pasient pasient, int reit){
    super(legemiddel, lege, pasient, reit);
  }


  @Override
  public String farge(){
    return "blaa"; // siden alle blå resepter er blå skriver jeg blå her
  }

  @Override
  public int prisAaBetale(){
    pris = legemiddel.hentPris();
    double kommapris = pris;
    double prisen = kommapris * 0.25;
    pris = (int) Math.round(prisen);
    return pris;

  }

  @Override
  public String toString(){
    return "Resept::: "+ farge +"-Resept \n  " + legemiddel +" \n  "+
      lege +" \n  Pasient: " + pasient.PasientNavn() + " - Fodselsnummer: " + pasient.Fodselsnummer() + "\n  Reit: "+ reit +
      "\n  Pris å betale: " + prisAaBetale() + "kr";
  }
}
