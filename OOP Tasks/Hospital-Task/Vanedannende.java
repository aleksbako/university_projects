public class Vanedannende extends Legemidler{
  int styrke;

  public Vanedannende(String navn, int pris, double virkestoff, int styrke){
    super(navn, pris, virkestoff);
    this.styrke = styrke;
  }

  public int hentVanedannendeStyrke(){
    return styrke;
  }

  @Override
  public String toString() {
    return ("Legemiddel: " + navn + " - ID: " + ID + " - Pris: " + pris + " - Virkestoff: " + virkestoff + " - Vanedannende Styrke: " + styrke);
  }
}
