/**
 * A implementation of a linked list which is always sorted.
 * @param <T> - uses generic data types.
 */
public class SortertLenkeliste<T extends Comparable<T>> extends Lenkeliste<T>{
    /**
     * Throws unsuportedOperatorException to not ruin the sorted part of the list.
     * @param pos - position of node in the list
     * @param x - the data of the node which will be placed
     */
    @Override
    public void leggTil(int pos, T x) {
            throw new UnsupportedOperationException();
    }

    /**
     * Adds element to the list and puts it in the position where it fits.
     * @param x - the data of the newly added node.
     */
    @Override
    public void leggTil(T x){
        //Check if we have a starting node.
        if(start == null){
            start = new Node(x);
        }

        else {

            //Go through list, if you find value that node that is bigger than it then add it behind it.
            int position = -1;
            int size = this.stoerrelse();
            for(int i=0;i<size;i++) {
                if (hent(i).compareTo(x) > 0) {
                    super.leggTil(i, x);
                    return;
                }

            }
            //If there doesn't exist a value that is bigger than it we add it to the end
            if(position == -1){
                position = size;
            }
            super.leggTil(position,x);
        }

}

    /**
     * Removes the largest value element from the list and returns  its data.
     * @return return data of the largest element.
     */
    @Override
    public T fjern(){
        int temp = 0;
        int size = this.stoerrelse();
        for(int i = 1; i < size; i++ ){
            if(hent(temp).compareTo(hent(i)) < 0){
                temp = i;
            }
        }
        return fjern(temp);

    }

    /**
     * Throws unsuportedOperatorException to not ruin the sorted part of the list.
     * @param pos - position of node in the list
     * @param x - the data of the node which will be placed
     */
    @Override
    public void sett(int pos, T x){
        throw new UnsupportedOperationException();
    }


}
