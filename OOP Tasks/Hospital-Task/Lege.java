
public class Lege implements Comparable<Lege>{
	protected String navn;
	public Lenkeliste<Resepter> utskrevendeResepter = new Lenkeliste<Resepter>();

	  public Lege(String navn){
	    this.navn = navn;
	  }
	  public String hentNavn(){
	    return navn;
	  }

	  @Override
	  public String toString(){
	    return "Lege: " + navn;
	  }

	  public int compareTo(Lege annenLege) {
		  if (this.navn.compareTo(annenLege.hentNavn()) < 0) {
			  return -1;
		  }if (this.navn.compareTo(annenLege.hentNavn()) > 0) {
			  return 1;
		  }
		  return 0;
	  }
	  public Lenkeliste<Resepter> hentUtsekrevendeResepter(){ // returnerer lenkeliste
		  return utskrevendeResepter;
	  }

	  public HviteResepter skrivHvitResept (Legemidler legemiddel, Pasient pasient, int reit) throws UlovligUtskrift{
			if (legemiddel instanceof Narkotisk && this instanceof Spesialist == false) {
				throw new UlovligUtskrift(this, legemiddel);

			}
			HviteResepter nyResept = new HviteResepter (legemiddel, this, pasient, reit);
			utskrevendeResepter.leggTil(nyResept);
			return nyResept;

	}
	public MilitaerResepter skrivMillitaerResept(Legemidler legemiddel, Pasient pasient, int reit) throws UlovligUtskrift{
		System.out.println("TESTING");
		if ((legemiddel instanceof Narkotisk) && !(this instanceof Spesialist)) {
			System.out.println("TESTING isnide");
			throw new UlovligUtskrift(this, legemiddel);
		}
		MilitaerResepter nyResept = new MilitaerResepter(legemiddel, this,  pasient, reit);

		utskrevendeResepter.leggTil(nyResept);
		return nyResept;

	}
	public PResepter skrivPResept(Legemidler legemiddel, Pasient pasient) throws UlovligUtskrift{
		if (legemiddel instanceof Narkotisk && (this instanceof Spesialist == false)) {
			throw new UlovligUtskrift(this, legemiddel);
		}
		PResepter nyResept = new PResepter(legemiddel, this,  pasient);
		utskrevendeResepter.leggTil(nyResept);
		return nyResept;
	}
	public BlaResepter skrivBlaaResept(Legemidler legemiddel, Pasient pasient, int reit) throws UlovligUtskrift{
		if (legemiddel instanceof Narkotisk && this instanceof Spesialist == false) {
			throw new UlovligUtskrift(this, legemiddel);
		}
		BlaResepter nyResept = new BlaResepter(legemiddel, this,  pasient, reit);
		utskrevendeResepter.leggTil(nyResept);
		return nyResept;
	}

}
