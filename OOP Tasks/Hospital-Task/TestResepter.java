public class TestResepter{

  public static void main(String[] args) {

    //oppretter et par legemidler, leger og respter for å teste
    Narkotisk nakotiskLegemiddel = new Narkotisk("Valium", 1000, 0.2, 1000);
    Vanedannende vanedannendeLegemiddel = new Vanedannende("Morfin", 90, 0.5, 500);
    VanligeLegemidler vanligLegemiddel = new VanligeLegemidler("Kepra", 500, 2);
    VanligeLegemidler Ppille = new VanligeLegemidler("Cerazette", 109, 20);

    Lege lege = new Lege("Ola Nordmann");

    Pasient p1 = new Pasient("Kari", "20039188297");

    BlaResepter nyBlaResept = new BlaResepter(nakotiskLegemiddel, lege, p1, 3);
    System.out.println(nyBlaResept.toString());

    PResepter nyPResept = new PResepter(Ppille, lege, p1);
    System.out.println(nyPResept.toString());
  }
}
