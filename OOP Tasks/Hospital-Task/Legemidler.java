import java.util.ArrayList;

public abstract class Legemidler{
  protected String navn;
  protected int pris;
  protected double virkestoff;
  protected int ID;
  protected static int teller = 1; // denne skal holde styr på IDen til legemiddelet.

  public Legemidler(String navn, int pris, double virkestoff){
    this.navn = navn;
    this.pris = pris;
    this.virkestoff = virkestoff;
    ID = teller; // setter IDen til å være lik 1
    teller ++;

  }

  public int hentID(){
    return ID;
  }

  public String hentNavn(){
    return navn;
  }

  public double hentVirkestoff(){
    return virkestoff;
  }

  public int hentPris(){
    return pris;
  }

  public void settNyPris(int nyPris){
    pris = nyPris;
  }

  public String toString() {
  return ("Legemiddel: " + navn + " - ID: " + ID + " - Pris: " + pris + " - Virkestoff mg: " + virkestoff);
  }
}
