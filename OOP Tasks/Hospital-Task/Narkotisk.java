public class Narkotisk extends Legemidler{
  int styrke;

  public Narkotisk(String navn, int pris, double virkestoff, int styrke){
    super(navn, pris, virkestoff);
    this.styrke = styrke;
  }

  public int hentNarkotiskStyrke(){
    return  styrke;
  }

  @Override
  public String toString() {
    return ("Legemiddel: " + navn + " - ID: " + ID + " - Pris: " + pris + " - Virkestoff: " + virkestoff + " - Narkotisk Styrke: " + styrke);
  }

}
