import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Reads File, used such that we can read as many files as we can using threads.
 */
public class Filereader implements Runnable{
    // length of the sequence
    static int subLengde = 3;

    private String filnavn;
    private HashBeholder alleHashMaps;

    /**
     *
     * @param filename - name of the file
     * @param alleHashMaps - the Hashbeholder of either positive or negative tested people.
     */
    public Filereader(String filename,  HashBeholder alleHashMaps){
        this.filnavn = filename;
        this.alleHashMaps = alleHashMaps;
    }

    /**
     * Reads file and adds all hashmaps to the allHashMaps object.
     */
    @Override
    public void run() {
        String linje, subStreng;
        try {
            Scanner leser =
                    new Scanner(new File("Data/" + filnavn));

            HashMap<String,SubSequence> subSeqHash = new HashMap <> ();
            System.out.println(" Virussjekker leser fil   " + filnavn );


            while(leser.hasNextLine()) {
                linje = leser.nextLine();
                linje = linje.trim();
                for (int ind = 0; ind + subLengde <= linje.length(); ind ++) {
                    subStreng = linje.substring(ind,ind+subLengde);
                    subSeqHash.putIfAbsent(subStreng,new SubSequence(subStreng));
                }
            }
            leser.close();
            alleHashMaps.leggTil(subSeqHash);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }


    }
}
