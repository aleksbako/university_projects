import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;
import java.util.HashMap;


public class VirusHoved {

    static String metadataFilbane = "Data/metadata.csv";//Location of the file with filenames.

    public static void main (String [ ] args) throws IOException {	
        String linje;

        HashBeholder haddeVirus = new HashBeholder();
        HashBeholder ikkeHaddeVirus = new HashBeholder();
        int threadcount = 1; // set threadcount to be 1 as default.
        // int antallFiler = 0;
        ArrayList<Thread> threads = new ArrayList<>();
        try{
            System.out.println(" Velkommen til HashBeholder test hovedprogram ");
            System.out.println("Please write how many threads do you want to be to minimize HashMap holders: ");
            Scanner threadcounter = new Scanner(System.in);
            threadcount = threadcounter.nextInt();
            Scanner leser = 
                new Scanner(new File(metadataFilbane));			

           int count = 0;

           //Read each line in file with filenames and send them to their own thread which adds them to the hashmap storage objects.
            while(leser.hasNextLine()) {
                if(count == 0){
                    linje = leser.nextLine();
                    count++;
                }
               else{
                    try {
                        linje = leser.nextLine();
                        linje = linje.trim();
                        String[] temp =  linje.split(",");
                        Filereader fileR = null;
                        //Checks if data is based on infected people, if yes then add to infectedobject while reading in the file through a thread.
                        if(Boolean.parseBoolean(temp[1])) {
                             fileR = new Filereader(temp[0], haddeVirus);
                            Thread thread = new Thread(fileR);
                            thread.start();
                            threads.add(thread);

                        }
                        else{
                            fileR = new Filereader(temp[0], ikkeHaddeVirus);
                            Thread thread = new Thread(fileR);
                            thread.start();
                            threads.add(thread);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
            leser.close();
        } catch (IOException e) {System.out.println(e.getMessage()); }
        System.out.println( );
        try {
            //Join each thread to the main thread such that we don't rush ahead before the threads are finished.
            for (Thread t : threads) {
                t.join();
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }


         // Reduce the Hashmap storing object of people who didn't have the virus to size to 1.

        while(haddeVirus.antall() > 1){
            threads = new ArrayList<>();
            for(int i = 0; i< threadcount;i++){
                try {

                        HashMap<String, SubSequence> Hash1 = haddeVirus.hentEn();
                        HashMap<String, SubSequence> Hash2 = haddeVirus.hentEn();
                        HashConcat hash = new HashConcat(haddeVirus, Hash1, Hash2);
                        Thread temp = new Thread(hash);
                        temp.start();
                        threads.add(temp);
                    //If the amount of threads already created iss bigger than the amount of object, break and go join the threads.
                    if(haddeVirus.antall() <= threadcount-i){

                        break;
                    }
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            try {
                //Join each thread to the main thread such that we don't rush ahead before the threads are finished.
                for (Thread t : threads) {

                    t.join();


                }


            }  catch (InterruptedException e){
                e.printStackTrace();
            }

        }


        //Reduce the Hashmap storing object of people who didn't have the virus to size to 1.
        while(ikkeHaddeVirus.antall() > 1){
            threads = new ArrayList<>();
            for(int i = 0; i< threadcount;i++){
                try {

                        HashMap<String, SubSequence> Hash1 = ikkeHaddeVirus.hentEn();
                        HashMap<String, SubSequence> Hash2 = ikkeHaddeVirus.hentEn();
                        HashConcat hash = new HashConcat(ikkeHaddeVirus, Hash1, Hash2);
                        Thread temp = new Thread(hash);
                        temp.start();
                        threads.add(temp);
                    //If the amount of threads already created iss bigger than the amount of object, break and go join the threads.
                    if(ikkeHaddeVirus.antall() <= threadcount-i){

                        break;
                    }

                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
            }

            try {
                //Join each thread to the main thread such that we don't rush ahead before the threads are finished.
                for (Thread t : threads) {
                    t.join();
                }
            }

            catch (InterruptedException e){
                e.printStackTrace();
            }
        }




        BinomialTest binomialTest = new BinomialTest();
        HashMap<String, SubSequence> hSyk2 = null;
        HashMap<String, SubSequence> hSyk = null;
        //Runs the simple test on the given dataset.
        System.out.println("Simple test result");
        try {
            hSyk2  = ikkeHaddeVirus.hentEn();
            hSyk =  haddeVirus.hentEn();
            for(SubSequence seq1 : hSyk.values()){
                for(SubSequence seq2: hSyk2.values()){

                    if(seq1.nokkel().equals(seq2.nokkel()) && (seq1.antall() - seq2.antall()) >= 5 ){

                        System.out.println(seq1.nokkel() + ", med person som hadde virus med antall: "  + seq1.antall() + ", " + seq2.nokkel() + " med person som ikke hadde virus med antall: " + seq2.antall() + ", og forskjel: " +  (seq1.antall() - seq2.antall()));
                    }

                }
            }
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
        //Runs the Binomial test on the given dataset. CAN TAKE A BIT OF TIME!
        System.out.println("");
        System.out.println("\n Binomial test results");

        for(SubSequence seq1 : hSyk.values()){
            for(SubSequence seq2: hSyk2.values()){
                if(seq1.nokkel().equals(seq2.nokkel()) && binomialTest.test((seq1.antall()+seq2.antall()), seq1.antall()) < 0.05){
                    System.out.println(seq1.nokkel() + ", med person som hadde virus med antall: "  + seq1.antall() + ", med person som ikke hadde virus med antall: " + seq2.antall() + ", og forskjel: " +  (seq1.antall() - seq2.antall()));
                }
            }
        }


        System.out.println(" FERDIG. Programmet terminerer. ");
    }



}
