import java.util.HashMap;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class HashBeholder {
    Lock lock = new ReentrantLock();
    private Condition notEmpty = lock.newCondition();
    ArrayList<HashMap<String,SubSequence> > alleH = 
        new ArrayList<HashMap<String,SubSequence> >(); 
    int  antall = 0;

    static HashMap<String,SubSequence> flett (HashMap<String,SubSequence> subSeqHash1,
            HashMap<String,SubSequence>  subSeqHash2){

        SubSequence hentaSub;  
        HashMap<String,SubSequence> subSeqHashNy = new HashMap<String,SubSequence> ();
        for(SubSequence  sub1:  subSeqHash1.values()) {
            hentaSub = subSeqHash2.remove(sub1.nokkel());
            if (hentaSub == null) {
                subSeqHashNy.put(sub1.nokkel(), sub1);
            }
            else {    				   
                int ant = hentaSub.antall();
                sub1.leggTil(ant);
                subSeqHashNy.put(sub1.nokkel(), sub1);  			
            }
        }   
        // Legger inn resten av subSeqHash2:		    			    		
        for(SubSequence  sub2:  subSeqHash2.values()) {
            subSeqHashNy.put(sub2.nokkel(),sub2);	
        }			 							
        return 	subSeqHashNy;					
    }

    public void testHele () {
        System.out.println(" Utskrift av antallet i hver hashmap i beholderen; ");
        for  (HashMap<String,SubSequence> hashN: alleH)
            System.out.println(" Storrelsen av HashMap:  " + hashN.size() );
    } 

    public int antall( ) {return antall;}

    public void leggTil(HashMap<String,SubSequence> hInn) {
        lock.lock();
        try {
            antall++;
            alleH.add(hInn);
            notEmpty.signalAll();
        }
        finally {
            lock.unlock();
        }
    }

    public HashMap<String,SubSequence> hentEn () throws InterruptedException {
        lock.lock();
        try {
            if (antall == 0) {
                notEmpty.await();
            }
            antall--;
            return alleH.remove(0);

        }
        finally {
            lock.unlock();
        }
    }
}
