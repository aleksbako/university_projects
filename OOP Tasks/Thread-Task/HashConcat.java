import java.util.HashMap;

/**
 * Concatinates 2 Hashmaps through threads.
 */
public class HashConcat implements Runnable{
    private HashBeholder allehash;
    private HashMap<String,SubSequence> subSeqHash1;
    private HashMap<String,SubSequence>  subSeqHash2;

    /**
     *
     * @param hashBeholder - Object which holds all hashmaps.
     * @param subSeq1 - first subsequence hashmap which will be combined.
     * @param subSeq2 - second subsequence hashmap which will be combined.
     */
    public HashConcat(HashBeholder hashBeholder,HashMap<String,SubSequence> subSeq1, HashMap<String,SubSequence>  subSeq2){
       this.allehash =  hashBeholder;
       this.subSeqHash1 = subSeq1;
       this.subSeqHash2 = subSeq2;
    }

    /**
     * Concatinates 2 hashmaps together into 1 using threads.
     */
    @Override
    public void run() {
        HashMap<String,SubSequence> subSeqHashNy = new HashMap<String,SubSequence> ();
        for(String seq1 : subSeqHash1.keySet()) {

                int s = subSeqHash1.get(seq1).antall();
                for (String seq2 : subSeqHash2.keySet()) {
                    if (seq1.equals(seq2)) {
                        s += subSeqHash2.get(seq2).antall();

                    }
                    else if(!subSeqHashNy.containsKey(seq2)){
                        //Adds the elements from seq2 hashmap that aren't in seq1 hashmap and only adds them once.
                        subSeqHashNy.put(seq2, subSeqHash2.get(seq2));
                    }
                }
                SubSequence tempSeq = new SubSequence(seq1);
                tempSeq.leggTil(s);
                subSeqHashNy.put(seq1, tempSeq);
            }
            allehash.leggTil(subSeqHashNy);

    }
}
