import matplotlib.pyplot as plt
import time
import scipy.signal as im
import numpy as np
from PIL import Image
from scipy.fftpack import dct

#Step 1
filename = 'uio.png'
f = np.array(Image.open(filename).convert('I'))



#Step 2.
for i in range(0,len(f)):
    for y in range(0,len(f[i])):
        f[i,y] = (f[i,y] - 128)


plt.imshow(f,cmap='gray')
plt.show()



#Step 3.
#Performs the discrete cosine transformation on an image.
def dct2d(image,f):
    for v in range(0,image.shape[0]):
        for u in range(0,image.shape[1]):
                for y in range(0,8):
                    for x in range(0,8):
                        image[v,u] = image[v,u] + (f[y,x] * np.cos(((2*x + 1)*u*np.pi)/16) * np.cos(((2*y + 1)*v*np.pi)/16))
                image[v,u] = image[v,u]*((1/4)*c(u)*c(v))
    return image
#Performs the inverse discrete cosine transformation on a transformed image.
def idct2d(image,f):
    for y in range(0,image.shape[0]):
        for x in range(0,image.shape[1]):
            for v in range(0,7):
                for u in range(0,7):
                    image[y,x] = image[y,x] + (c(u)*c(v)*f[v,u] * np.cos(((2*x + 1)*u*np.pi)/16) * np.cos(((2*y + 1)*v*np.pi)/16))
            image[y,x] = int(round((1/4) * image[y,x]))
    return image

#decides the value for cosine and inverse cosine transformations.
def c(a):
    if a == 0:
        return 1/np.sqrt(2)
    return 1

block_img = np.zeros(f.shape)
im_h, im_w = f.shape
bl_h, bl_w = 8, 8
#Splits image into blocks and performs the cosine transformation.
for row in np.arange(im_h - bl_h + 1, step=bl_h):
    for col in np.arange(im_w - bl_w + 1, step=bl_w):
        block_img[row:row+bl_h, col:col+bl_w] = dct2d(block_img[row:row+bl_h, col:col+bl_w],f[row:row+bl_h, col:col+bl_w])

plt.imshow(block_img,cmap='gray')
plt.show()


#Step 4
#Splits image into blocks and performs the inverse cosine transformation.
im_h, im_w = block_img.shape
invnew_image = np.zeros((block_img.shape))
for row in np.arange(im_h - bl_h + 1, step=bl_h):
    for col in np.arange(im_w - bl_w + 1, step=bl_w):
        invnew_image[row:row+bl_h, col:col+bl_w] = idct2d(invnew_image[row:row+bl_h, col:col+bl_w],block_img[row:row+bl_h, col:col+bl_w])


plt.imshow(invnew_image,cmap='gray')
plt.show()

for y in range(len(invnew_image)):
    for x in range(len(invnew_image[y])):
        invnew_image[y][x] = int(invnew_image[y][x] + 128)


plt.imshow(invnew_image,cmap='gray')
plt.show()


#Step 5.
#quantization matrix.
Q = np.array([
[16,11,10,16,24,40,51,61],
[12,12,14,19,26,58,60,55],
[14,13,16,24,40,57,69,56],
[14,17,22,29,51,87,80,62],
[18,22,37,56,68,109,103,77],
[24,35,55,64,81,104,113,92],
[49,64,78,87,103,121,120,101],
[72,92,95,98,112,100,103,99]])

#quantizes image.
def quantize(image,q,Q):
    newimage = np.zeros(image.shape)
    im_h, im_w = image.shape
    bl_h, bl_w = 8, 8
    for row in np.arange(im_h - bl_h + 1, step=bl_h):
        for col in np.arange(im_w - bl_w + 1, step=bl_w):
            newimage[row:row+bl_h, col:col+bl_w] = np.round(np.divide(image[row:row+bl_h, col:col+bl_w],(q*Q)))
    return newimage
#reconstruct the quantized image
def reconstruct(quantized_image, q, Q):
    newimage = np.zeros(quantized_image.shape)
    im_h, im_w = quantized_image.shape
    bl_h, bl_w = 8, 8
    for row in np.arange(im_h - bl_h + 1, step=bl_h):
        for col in np.arange(im_w - bl_w + 1, step=bl_w):
            newimage[row:row+bl_h, col:col+bl_w] = np.round(np.multiply(quantized_image[row:row+bl_h, col:col+bl_w],(q*Q)))
    return newimage

#Step 6.
quantized_image = quantize(block_img,1,Q)
reconstructed_image = reconstruct(quantized_image,1,Q)
newimage = np.zeros(reconstructed_image.shape)
for row in np.arange(im_h - bl_h + 1, step=bl_h):
    for col in np.arange(im_w - bl_w + 1, step=bl_w):
        newimage[row:row+bl_h, col:col+bl_w] = idct2d(newimage[row:row+bl_h, col:col+bl_w],reconstructed_image[row:row+bl_h, col:col+bl_w])

for i in range(0,len(newimage)):
    for y in range(0,len(newimage[i])):
        newimage[i,y] = (newimage[i,y] + 128)

plt.imshow(newimage,cmap='gray')
plt.show()

#Calculates the entropy of the given image.
def entropy(image):
    value,counts = np.unique(image, return_counts=True)
    N = counts.sum()
    p_i = counts / N

    H = 0
    for i in range(len(value)):
        H += (-p_i[i]*np.log2(p_i[i]))
    return H

#Calculates compression rate providing average amount of bits used(i.e entropy)
def compressionRate(c):
    #Since we have 8 bits as our constant amount of bits per symbol.
    return 8/c

r = entropy(quantized_image)

cr = compressionRate(r)
R = 1 - (1/cr)
print("q = 1: compression rate , Relative redundance, entropy ",cr, R,r)

#Step 7
q_values = np.array([0.1,0.5,2,8,32])
entropies = np.zeros(q_values.shape[0])
for q in range(len(q_values)):
    quantized_image = quantize(block_img,q_values[q],Q)
    reconstructed_image = reconstruct(quantized_image,q_values[q],Q)
    entropies = np.append(entropies,entropy(quantized_image))
    newimage = np.zeros(reconstructed_image.shape)
    for row in np.arange(im_h - bl_h + 1, step=bl_h):
        for col in np.arange(im_w - bl_w + 1, step=bl_w):
            newimage[row:row+bl_h, col:col+bl_w] = idct2d(newimage[row:row+bl_h, col:col+bl_w],reconstructed_image[row:row+bl_h, col:col+bl_w])

    for i in range(0,len(newimage)):
        for y in range(0,len(newimage[i])):
            newimage[i,y] = (newimage[i,y] + 128)
    plt.imshow(newimage,cmap='gray')
    plt.show()






#lists the compressed rate and relative redundance of all entropies for the given q values.
cr01 = compressionRate(entropies[5])
R01 = 1 - (1/cr01)
print("q = 0.1: compression rate ,Relative redundance, entropy ", cr01,R01,entropies[5])

cr05 = compressionRate(entropies[6])
R05 = 1 - (1/cr05)
print("q = 0.5: compression rate ,Relative redundance, entropy ", cr05,R05,entropies[6])

cr2 = compressionRate(entropies[7])
R2 = 1 - (1/cr2)
print("q = 2: compression rate ,Relative redundance, entropy ", cr2,R2,entropies[7])

cr8 = compressionRate(entropies[8])
R8 = 1 - (1/cr8)
print("q = 8: compression rate , Relative redundance, entropy ", cr8,R8,entropies[8])

cr32 = compressionRate(entropies[9])
R32 = 1 - (1/cr32)
print("q = 32: compression rate , Relative redundance, entropy", cr32,R32,entropies[9])
