import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as im
from PIL import Image
#<---------------------------- Oppgave 1 --------------------------->



#--------------------------------- Helper functions ---------------------------



def bilinearInterp(x,y,image):

    x2 = int(np.ceil(x))
    x1 = int(np.floor(x))
    y1 = int(np.floor(y))
    y2 = int(np.ceil(y))
    P = 0
    if ((x2-x1) != 0 and (y2-y1) != 0) and y2 < len(image) and y1 < len(image):
        R1 = ((x2 - x)/(x2 - x1))*image[y1,x1] + ((x - x1)/(x2 - x1))*image[y1,x2]
        R2 = ((x2 - x)/(x2 - x1))*image[y2,x1] + ((x - x1)/(x2 - x1))*image[y2,x2]
        P = ((y2 - y)/(y2 - y1))*R1 + ((y - y1)/(y2 - y1))*R2
    else:
        P = image[y1,x1]
    return P



filename = 'portrett.png'
f = np.array(Image.open(filename).convert('L'))


#Adjust Image mean and standard deviation.
T = np.zeros(255)
a = 64/f.std()
b = 127 - a*f.mean()
for i in range(255):
    T[i] = 127 + (i - f.mean())*a

newimage = np.zeros(f.shape)

for i in range(len(f)):
    for j in range(len(f[i])):
        newimage[i,j] = T[f[i,j]]

print(newimage.mean())
print(newimage.std())
plt.imshow(newimage,cmap='gray')
plt.show()

#Bilinear interpelation
outputBilinear = np.zeros((600,512))
srcH , srcW = newimage.shape
for i in range(0,600-1):
    for j in range(0,512-1):
        y= i*(srcH/600)
        x= j*(srcW/512)
        effect = bilinearInterp(x,y,newimage)
        outputBilinear[i][j] = effect

plt.imshow(outputBilinear,cmap='gray')
plt.show()


#Nearest neighbor interpelation
outputB = np.zeros((600,512))
srcH , srcW = newimage.shape
for i in range(600-1):
    for j in range(512-1):
            y=round(i*(srcH/600))
            x=round(j*(srcW/512))
            if x >= 0 and y >= 0 and y < len(newimage) and x < len(newimage[0]):
                outputB[i][j] = newimage[y,x]


plt.imshow(outputB,cmap='gray')
plt.show()

filename = 'geometrimaske.png'
fi = np.array(Image.open(filename).convert('L'))


xr = np.array([259,259,443])
yr = np.array([169,342,250])


G = np.array([[150,312,1],[211,237,1],[228,383,1]])
M = np.dot(np.linalg.inv(np.dot(G.T,G)),G.T)
b = np.dot(M,xr)
a = np.dot(M,yr)

#Forwards Mapping.
transformedB = np.zeros((600,512))
for i in range(600-1):
    for j in range(512-1):
        newx = a[0]*j + a[1] * i + a[2]
        newy = b[0]*j + b[1] * i + b[2]
        newx = int(newx)
        newy = int(newy)
        if newx >= 0 and newy >= 0 and newy < len(outputB) and newx < len(outputB[i]):
            transformedB[newy][newx] = outputB[i,j]

plt.imshow(transformedB,cmap='gray')
plt.imshow(fi,cmap='gray', alpha=0.1)
plt.show()

#Backwards nearest neighbor interpelation
inversetransf = np.zeros((600,512))
T = np.array([[a[0],a[1],a[2]],[b[0],b[1],b[2]],[0,0,1]])
Tinv = np.linalg.inv(T)
for i in range(600-1):
    for j in range(512-1):
        newx = Tinv[0][0]*j + Tinv[0][1] * i + Tinv[0][2]
        newy = Tinv[1][0]*j + Tinv[1][1] * i + Tinv[1][2]
        newx = int(newx)
        newy = int(newy)
        if newx >= 0 and newy >= 0 and newy < len(outputB) and newx < len(outputB[i]):
            inversetransf[i][j] = outputB[newy,newx]

plt.imshow(inversetransf,cmap='gray')
plt.imshow(fi,cmap='gray', alpha=0.2)
plt.show()

#Backwards bilinear interpelation
Bilinearinversetransf = np.zeros((600,512))
T = np.array([[a[0],a[1],a[2]],[b[0],b[1],b[2]],[0,0,1]])
Tinv = np.linalg.inv(T)
for i in range(600-1):
    for j in range(512-1):
        newx = Tinv[0][0]*j + Tinv[0][1] * i + Tinv[0][2]
        newy = Tinv[1][0]*j + Tinv[1][1] * i + Tinv[1][2]
        newx = int(newx)
        newy = int(newy)
        if newx >= 0 and newy >= 0 and newy < len(outputBilinear) and newx < len(outputBilinear[i]):
            Bilinearinversetransf[i][j] = outputBilinear[newy,newx]

plt.imshow(Bilinearinversetransf,cmap='gray')
plt.imshow(fi,cmap='gray', alpha=0.2)
plt.show()


#<----------------------------------- Oppgave 2 -------------------------------->



filename = 'cellekjerner.png'
f = np.array(Image.open(filename).convert('L'))

#Zero padding, helper function for convolution
def padding(img,filter):
    h = filter.shape[0]
    paddingsize = int(np.floor(h/2))
    paddedimage = np.zeros((img.shape[0]+2*paddingsize, img.shape[1]+2*paddingsize))
    for i in range(paddingsize, img.shape[0]+paddingsize):
        for j in range(paddingsize,img.shape[1]+paddingsize):
            paddedimage[i,j] = img[i-paddingsize,j-paddingsize]
    return paddedimage


#Convolution, only percise with odd numbered filters.
def convolution(img, filter):
    #filter = np.flip(filter,1)
    paddingsize = int(np.floor(filter.shape[0]/2))
    newimage = padding(img,filter)
    imageRows , imageColumns = newimage.shape

    output = np.zeros(newimage.shape)

    for y in range(imageRows):
        for x in range(imageColumns):
            for t in range(filter.shape[0]):
                for s in range(filter.shape[1]):
                    if (y-t >= 0) and (y-t < imageRows ) and (x-s >= 0) and (x-s < imageColumns):
                        output[y,x] = output[y,x] + filter[t,s]*newimage[y-t,x-s]


    #Get values by removing unnecessary values from output array.
    result = np.zeros(img.shape)
    for y in range(paddingsize*2,imageRows):
        for x in range(paddingsize*2,imageColumns):
            result[y-paddingsize*2,x-paddingsize*2] = output[y,x]


    return result



#Creates a gauss low pass filter.
def gaussfilter(sigma):
    size = 1+int(round(8*sigma))
    x, y = np.mgrid[-0:size , -0:size]
    A = 1 / (2 * np.pi * sigma**2)
    g = A * np.exp(-((x**2 + y**2) / (2*sigma**2)))
    return g



#Step 1. Filter with gauss filter
gaus = gaussfilter(5)

filtered_image = convolution(f,gaus)
#filtered_image2 = im.convolve2d(f,gaus,mode='same')

#Im convolve2d returns the same as convolution. Uncomment this to check if it holds.
#equal = filtered_image == filtered_image2
#print(equal.all())

#plt.imshow(filtered_image,cmap='gray')
#plt.show()

#Step 2. Find gradient magnitude and direction.
symm1dx = np.array([[0,1,0],[0,0,0],[0,-1,0]])
symm1dy = np.array([[0,0,0],[1,0,-1],[0,0,0]])

temp1 = convolution(filtered_image,symm1dx)
#temp1 = im.convolve2d(filtered_image,symm1dx,mode='same')

temp2 = convolution(filtered_image,symm1dy)
#temp2 = im.convolve2d(filtered_image,symm1dy,mode='same')

magnitude = np.sqrt(temp1**2 + temp2**2)
direction = np.arctan2(temp2,temp1)
angle = np.rad2deg(direction)

#plt.imshow(magnitude,cmap='gray')
#plt.show()

#plt.imshow(direction,cmap='gray')
#plt.show()



#Step 3. remove magnitude values which are smaller than a other magnitudes which are pointing in a certain direction.
edge_detect = np.zeros(magnitude.shape)

for i in range(1,magnitude.shape[0]-1):
    for j in range(1,magnitude.shape[1]-1):
        a = 255
        b = 255
        if (0 <= angle[i,j] < 22.5) or (157.5 <= angle[i,j] <= 180) or (-22.5 <= angle[i,j] < 0) or (-180 <= angle[i,j] < -157.5):
            a = magnitude[i, j+1]
            b = magnitude[i, j-1]

        elif (22.5 <= angle[i,j] < 67.5) or (-157.5 <= angle[i,j] < -112.5):
            a = magnitude[i+1, j+1]
            b = magnitude[i-1, j-1]

        elif (67.5 <= angle[i,j] < 112.5) or (-112.5 <= angle[i,j] < -67.5):
            a = magnitude[i+1, j]
            b = magnitude[i-1, j]

        elif (112.5 <= angle[i,j] < 157.5) or (-67.5 <= angle[i,j] < -22.5):
            a = magnitude[i+1, j-1]
            b = magnitude[i-1, j+1]

        if (magnitude[i,j] >= a) and (magnitude[i,j] >= b):
            edge_detect[i,j] = magnitude[i,j]
        else:
            edge_detect[i,j] = 0

#plt.imshow(edge_detect,cmap='gray')
#plt.show()

#Step 4. Thresholding.
Th = 5
Tl = 1


M, N = edge_detect.shape
output = np.zeros(edge_detect.shape)

max_i, max_j = np.where(edge_detect >= Th)
zeros_i, zeros_j = np.where(edge_detect < Tl)
weak_i, weak_j = np.where((edge_detect < Th) & (edge_detect >= Tl))

output[max_i, max_j] = 255
output[zeros_i, zeros_j ] = 0
output[weak_i, weak_j] = 75

temp = edge_detect
checker = temp == output

while checker.all() == False:
    temp = output.copy()

    for i in range(1, M-1):
        for j in range(1, N-1):
            if (output[i,j] == 75):
                if 255 in [output[i+1, j-1],output[i+1, j],output[i+1, j+1],output[i, j-1],output[i, j+1],output[i-1, j-1],output[i-1, j],output[i-1, j+1]]:
                    output[i, j] = 255

    checker = temp == output



for i in range(0, M-1):
    for j in range(0, N-1):
        if (output[i,j] == 75):
            output[i,j] = 0

plt.imshow(output,cmap='gray')
plt.show()
