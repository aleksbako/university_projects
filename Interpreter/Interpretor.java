import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Oblig1 : Main Part of the Program.
 */
class Oblig1 {
    public static void main(String[] args) {
        TestCode testCode = new TestCode();
        switch (args.length > 0 ? args[0] : "") {
            case "1":
                testCode.runProgram1();
                return;
            case "2":
                testCode.runProgram2();
                return;
            case "3":
                testCode.runProgram3();
                return;
            case "4":
                testCode.runProgram4();
                return;
            case "all":
                testCode.runAll();
                return;
            default:
                System.out.println("USAGE: java Oblig1 1|2|3|4|all");
                return;
        }
    }
}

/**
 * TestCode : Contains Test programs that are based on AST.
 */
class TestCode {

    // Create the AST based on testing code 1
    // This code is just to help you understand how to create an AST
    void runProgram1() {
        Grid grid = new Grid(new NumberExp(64), new NumberExp(64));
        Start start = new Start(new NumberExp(23), new NumberExp(30));
        List<Statement> statements = new ArrayList<>();
        statements.add(new Move(Direction.west, new NumberExp(15)));
        statements.add(new Move(Direction.south, new NumberExp(15)));
        statements.add(new Move(Direction.east, new Plus(new NumberExp(2),new NumberExp(3))));
        statements.add(new Move(Direction.north, new Plus(new NumberExp(17),new NumberExp(20))));
        statements.add(new Stop());
        Robot robot = new Robot(null,statements,start);
        Program program = new Program(grid,robot);
        // Fill in rest of the code

        // Run the interpreter
        program.interpret();

    }

    // same as runProgram1 but with the AST based on the other example programs
    void runProgram2() {
        Grid grid = new Grid(new NumberExp(64), new NumberExp(64));
        List<Binding> bindings = new ArrayList<>();
        bindings.add(new Binding("i",new NumberExp(5)));
        bindings.add(new Binding("j",new NumberExp(3)));
        Start start = new Start(new NumberExp(23), new NumberExp(6));
        List<Statement> statements = new ArrayList<>();
        statements.add(new Move(Direction.north, new Star(new NumberExp(3),new Identifier("i"))));
        statements.add(new Move(Direction.east, new NumberExp(15)));
        statements.add(new Move(Direction.south, new Minus(new NumberExp(12) ,new Minus(new Identifier("i"),new Identifier("j")))));
        statements.add(new Move(Direction.west, new Plus(new Plus(new Star(new NumberExp(2),new Identifier("i"))
                ,new Star(new NumberExp(3),new Identifier("j"))),new NumberExp(1))));
        statements.add(new Stop());
        Robot robot = new Robot(bindings,statements,start);
        Program program = new Program(grid,robot);
        program.interpret();

    }
    void runProgram3() {
        Grid grid = new Grid(new NumberExp(64), new NumberExp(64));
        List<Binding> bindings = new ArrayList<>();
        bindings.add(new Binding("i",new NumberExp(5)));
        bindings.add(new Binding("j",new NumberExp(3)));
        Start start = new Start(new NumberExp(23), new NumberExp(6));
        List<Statement> statements = new ArrayList<>();
        statements.add(new Move(Direction.north, new Star(new NumberExp(3),new Identifier("i"))));
        statements.add(new Move(Direction.west, new NumberExp(15)));
        statements.add(new Move(Direction.east, new NumberExp(4)));
        statements.add(new Loop(new LessThan(new Identifier("j"),new NumberExp(1)), new ArrayList<Statement>(Arrays.asList(
                new Move(Direction.south, new Identifier("j")),
                new Assignment("j", new Minus(new Identifier("j"),new NumberExp(1))))
        )));
        statements.add(new Stop());
        Robot robot = new Robot(bindings,statements,start);
        Program program = new Program(grid, robot);
        program.interpret();
    }
    void runProgram4() {
        Grid grid = new Grid(new NumberExp(64), new NumberExp(64));
        List<Binding> bindings = new ArrayList<>();
        bindings.add(new Binding("i",new NumberExp(8)));
        Start start = new Start(new NumberExp(1), new NumberExp(1));
        List<Statement> statements = new ArrayList<>();
        statements.add(new Loop(new GreaterThan(new Identifier("i"),new NumberExp(100)), new ArrayList<Statement>(Arrays.asList(
                new Move(Direction.north, new Identifier("i"))
        ))));

        statements.add(new Stop());
        Robot robot = new Robot(bindings,statements,start);
        Program program = new Program(grid, robot);
        program.interpret();
    }
    void runAll() {
        runProgram1();
        runProgram2();
        runProgram3();
        runProgram4();
    }
}
// <---------------------------- Main Program ---------------------->

/**
 * Robol, Interface of the language.
 */
interface Robol {
    void interpret();
}

/**
 * Starts up the program
 */
class Program implements Robol {
    Grid grid;
    Robot robot;
    public Program(Grid grid, Robot robot) {
        this.grid = grid;
        this.robot = robot;
    }
    public void interpret() {
        robot.SetGrid(this.grid);
        robot.interpret();
    }
}

/**
 * Direction defines which way the robot may go.
 */
enum Direction{
    west,south,north,east;
}

/**
 * Robot, character that moves along the grid.
 */
class Robot implements Robol {
   private NumberExp Xposition;
    private NumberExp Yposition;
    private List<Statement> statementList;
    private List<Binding> bindingList;
    private Start start;

    private Grid grid;

    public Robot(List<Binding> bindings, List<Statement> statements, Start startclass){
        this.statementList = statements;
        this.start = startclass;
        this.bindingList = bindings;
    }
    public void interpret() {
        // write interpreter code for the robot here
        start.interpret(this);
        for(Statement s: statementList){
            s.interpret(this);
        }
    }
    public NumberExp GetXPosition(){
        return this.Xposition;
    }
    public NumberExp GetYPosition(){
        return this.Yposition;
    }
    public Grid GetGrid(){
        return this.grid;
    }
    public void SetGrid(Grid gr){
        this.grid = gr;
    }
    public void SetXPosition(NumberExp x){
        this.Xposition = x;
    }
    public void SetYPosition(NumberExp y) {
        this.Yposition = y;
    }
    public List<Binding> GetBindings(){
        return this.bindingList;
    }
    public void SetBindings(List<Binding> bindings){this.bindingList = bindings;}
}

/**
 * Put the robot in a specific position/
 */
class Start extends location{
        public Start(NumberExp x, NumberExp y){
           super(x,y);
        }
       public void interpret(Robot robot){
            robot.SetXPosition(super.GetXLocation());
            robot.SetYPosition(super.GetYLocation());
       }
}

// <--------------------------- Statements --------------------->

/**
 * Statement, sets up an abstract function of how to handle statements.
 */
abstract class Statement  {
    public abstract void interpret(Robot robot);
}

/**
 * Assignment statement, handles  assigning Expressions to Bindings.
 */
class Assignment extends Statement {
    private String id;
    private Expression expression;
    public Assignment(String ident, Expression exp){
        this.id = ident;
        this.expression = exp;
    }
    public void interpret(Robot robot) {

        try{
            List<Binding> bindings = robot.GetBindings();
            for(Binding bind : bindings){
                if(bind.GetId().equals(this.id)){
                        bind.SetExpression(this.expression.Check(robot));
                        robot.SetBindings(bindings);
                }
            }
            if (this.id == null) {
                throw new VariableNotFoundException(this.id);
            }

        }
        catch(VariableNotFoundException exception){
            System.out.println("No Variable found with given Identifier");
            exception.printStackTrace();
            System.exit(0);
        }

    }
}

/**
 * Loop : Loops through the provided statements until the condition is met.
 */
class Loop extends Statement {
    List<Statement> statements;
    BoolExp condition;
    public Loop(BoolExp boolexp, List<Statement> statementlist){
        this.condition = boolexp;
        this.statements = statementlist;
    }
    public void interpret(Robot robot) {
        for(Statement s: statements){
            s.interpret(robot);
        }
       int check = condition.Check(robot).GetValue();
        if(check == 0){
           interpret(robot);
        }

    }
}

/**
 * Stop : Stops the program and returns the position of the robot on the grid.
 */
class Stop extends Statement {
    public void interpret(Robot robot) {
        System.out.format("Robot stopped at position (%d, %d)\n",
                robot.GetXPosition().GetValue(), robot.GetYPosition().GetValue());
    }
}

/**
 * location : Has coordinates of where something is located.
 */
abstract class location {
    private NumberExp x,y;
    public location(NumberExp x_value,NumberExp y_value){
        x = x_value;
        y = y_value;
    }
    public NumberExp GetXLocation(){
        return x;
    }
    public NumberExp GetYLocation(){
       return y;
    }
}

/**
 * Move : Moves the robot in a certain direction. Throws Exception if robot attempts to leave the grid.
 */
class Move extends Statement{
        protected Direction dir;
        protected Expression exp;
        public Move(Direction direction, Expression Exp){
        this.dir = direction;
        this.exp = Exp;
    }
    public void interpret(Robot robot){

            try {
                int y = robot.GetYPosition().GetValue();
                int x = robot.GetXPosition().GetValue();
                Grid grid = robot.GetGrid();


                switch (this.dir) {
                    case north: if(!grid.CheckOutBound(robot.GetXPosition(),new NumberExp(y + exp.Check(robot).GetValue()))){
                        throw new OutOfBounds( robot.GetXPosition(), robot.GetYPosition());
                    }
                        robot.SetYPosition(new NumberExp(y + exp.Check(robot).GetValue()));break;

                    case south: if(!grid.CheckOutBound(robot.GetXPosition(),new NumberExp(y + exp.Check(robot).GetValue()))){
                        throw new OutOfBounds( robot.GetXPosition(), robot.GetYPosition());
                    }
                        robot.SetYPosition(new NumberExp(y - exp.Check(robot).GetValue())); break;

                    case west:
                        if(!grid.CheckOutBound(new NumberExp(x + exp.Check(robot).GetValue()),robot.GetYPosition())){
                            throw new OutOfBounds( robot.GetXPosition(), robot.GetYPosition());
                        }
                        robot.SetXPosition(new NumberExp(x - exp.Check(robot).GetValue())); break;

                    case east:
                        if(!grid.CheckOutBound(new NumberExp(x + exp.Check(robot).GetValue()),robot.GetYPosition())){
                            throw new OutOfBounds( robot.GetXPosition(), robot.GetYPosition());
                        }
                        robot.SetXPosition(new NumberExp(x + exp.Check(robot).GetValue())); break;
                }

            if(false){
                throw new OutOfBounds(robot.GetXPosition(),robot.GetYPosition());
            }
            }catch (OutOfBounds e){
                e.printStackTrace();
                System.exit(0);
            }
    }
        }

//<----------------------------------- Expressions----------------------------------->
        interface Expression {
    public NumberExp Check(Robot robot);}


/**
 * Bindings are variables in Robol.
 */
class Binding {
    private String id;
    private Expression exp;

    public Binding(String ident, Expression expression){
        this.id = ident;
        this.exp = expression;
    }

    public void SetExpression(Expression expression){
        this.exp = expression;
    }
    public Expression GetExpression(){
        return this.exp;
    }
    public String GetId(){
        return this.id;
    }

}

/**
 * Identifier Verifies if a Binding exist based on its id, and if it does, it returns it's value.
 */
class Identifier implements Expression{
    protected String id;
    public Identifier(String ident){
        this.id = ident;
    }

    public NumberExp Check(Robot robot){
        NumberExp value = new NumberExp(0);
        try{
            List<Binding> bindings = robot.GetBindings();

            for(Binding bind : bindings){
                if(bind.GetId().equals(this.id)){
                    value = (NumberExp) bind.GetExpression();

                }
            }
            if (this.id == null) {
                throw new VariableNotFoundException(this.id);
            }

        }
        catch(VariableNotFoundException exception){
            System.out.println("No Variable with Identifier found");
            exception.printStackTrace();
            System.exit(0);
        }
        return value;
    }



}

/**
 * BoolExp, is a Boolean Expression abstract class, which just creates the structure of boolean Expressions.
 */

abstract class BoolExp implements Expression {
    protected Expression left;
    protected Expression right;
    public BoolExp(Expression l, Expression r) {
        this.left = l;
        this.right = r;
    }
}

/**
 * LessThan Evaluates if the left expression is smaller than right.
 */
class LessThan extends BoolExp{
    public LessThan(Expression l, Expression r){
        super(l,r);
    }
    public NumberExp Check(Robot robot) {

        int value = left.Check(robot).GetValue() < right.Check(robot).GetValue() ? 1 : 0;

        return new NumberExp(value);
    }
}
/**
 * GreaterThan Evaluates if the left expression is larger than right.
 */
class GreaterThan extends BoolExp{
    public GreaterThan(Expression l, Expression r){
        super(l,r);
    }
    public NumberExp Check(Robot robot) {
        int value = left.Check(robot).GetValue() > right.Check(robot).GetValue() ? 1 : 0;

        return new NumberExp(value);
    }
}
/**
 * Equals Evaluates if the left expression is equal to the right.
 */
class Equals extends BoolExp{
    public Equals(Expression l, Expression r){
        super(l,r);
    }
    public NumberExp Check(Robot robot) {
        int value = left.Check(robot).GetValue() == right.Check(robot).GetValue() ? 1 : 0;

        return new NumberExp(value);
    }
}


/**
 * ArethamticExp, is a Arethmatic Expression abstract class, which just creates the structure of arithmatic Expressions to perform arithmatic operations.
 */
abstract class ArethamticExp implements Expression {
    protected Expression left;
    protected Expression right;
    public ArethamticExp(Expression l, Expression r){
        this.left = l;
        this.right = r;
    }
}


/**
 * Plus evaluates the expression and performs the plus operation on the left and right expression.
 */
class Plus extends ArethamticExp{
    public Plus(Expression left, Expression right){
        super(left,right);
    }

    public NumberExp Check(Robot robot) {

        return new NumberExp(left.Check(robot).GetValue() + right.Check(robot).GetValue());
    }
}
/**
 * Minus evaluates the expression and performs the minus operation on the left and right expression.
 */
class Minus extends ArethamticExp {
    public Minus(Expression left, Expression right) {
        super(left, right);
    }

    public NumberExp Check(Robot robot) {

        return new NumberExp(left.Check(robot).GetValue() - right.Check(robot).GetValue());
    }
}
/**
 * Star evaluates the expression and performs the multiplication operation on the left and right expression.
 */
class Star extends ArethamticExp {
    public Star(Expression left, Expression right) {
        super(left, right);
    }

    public NumberExp Check(Robot robot) {

        return new NumberExp(left.Check(robot).GetValue() * right.Check(robot).GetValue());
    }
}

/**
 * NumberExp : Integer operator in Robol.
 */
class NumberExp implements Expression{
    protected int number;
    public NumberExp(int value){
        number = value;
     }
     public int GetValue(){
        return this.number;
     }
     public NumberExp Check(Robot robot){
        return this;
     }

}

/**
 * The grid which the robot may traverse.
 */
class Grid {
    NumberExp xsize;
    NumberExp ysize;
    public Grid(NumberExp x, NumberExp y){
    xsize = x;
    ysize = y;
    }

    public boolean CheckOutBound(NumberExp newrbposx, NumberExp newrbposy){
        if(newrbposx.GetValue()<0 || newrbposx.GetValue() > this.xsize.GetValue() || newrbposy.GetValue() < 0 || newrbposy.GetValue() > this.ysize.GetValue()){
            return false;
        }
        return true;
    }

}

/**
 * OutOfBounds: if robot leaves grid then it casts this exception.
 */
class OutOfBounds extends Exception{
    public OutOfBounds(NumberExp x, NumberExp y) {
        super(String.format("OutOfBound at (%d, %d)", x.GetValue(),y.GetValue()));
    }
}
/**
 * VariableNotFoundException: if Binding doesn't exist in the robot's list of bindings, then cast this exception.
 */
class VariableNotFoundException extends Exception {
    public VariableNotFoundException(String identifier) {
        super(String.format("Variable \"%s\" was not found.", identifier));
    }
}

