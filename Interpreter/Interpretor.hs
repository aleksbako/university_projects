
type AssocList = [(String, Int)]
--- AssocList Operations
lookupAssoc :: String -> AssocList -> Maybe Int
lookupAssoc key assocList = foldr (checker) Nothing assocList where checker (x,y) Nothing = if key == x then (Just y) else Nothing; checker (x,y) a =  a
  
add :: String -> Int -> AssocList -> AssocList
add key value assocList = (key,value):assocList

change :: String -> Int -> AssocList -> AssocList
change key value assocList = map (\x@(f, _) -> if f == key then (key,value) else x) assocList

--- Converts Bindinglist to AssocList
convertbindtoassoc :: [Binding] -> [(String, Int)] -> [(String, Int)]
convertbindtoassoc (b:bindings) assocls = convertbindtoassoc bindings ((bindtoassoctuple b):assocls)
convertbindtoassoc [] assocls = assocls

bindtoassoctuple :: Binding -> (String, Int)
bindtoassoctuple (Binding (Identifier s ,Number numb)) =  (s, numb)

data Direction = North | West | East | South deriving Show
data Grid = Size Exp Exp
--- Abstract Data type of Expression
data Exp =
   BiggerThan Exp Exp
 | LessThan Exp Exp
 | Identifier String
 | Equals Exp Exp
 | Minus Exp Exp
 | Plus Exp Exp
 | Multiply Exp Exp
 | Number Int
 | Until Exp
 deriving Show
-- Abstract Data type of Statements
data Statement = 
    Move Direction Exp
   | Assignment Exp Exp
   | Loop [Statement] Exp
   | Stop
   deriving Show
   
   
data Binding = Binding (Exp, Exp)

data Start = Start (Exp, Exp)
--Abstract Data Type Robot
data Robot = Robot
  { robotBindings :: [Binding]
  , robotStart :: Start
  , robotStatements :: [Statement]
  }

data Program = Program Grid Robot

--Checks where the robot will be positioned after moving
checkbounds val maximumval = if (val < 0 || val > maximumval) then 1 else 0

removejust (Just a) = a
-- Throws RobolError if called
castError (VariableNotFound s) = error ("Variable " ++ s ++ " Not found")
castError  (OutOfBounds (x,y)) = error ("Out of Bounds at (" ++ (show x) ++ ", " ++ (show y) ++ ")")
--- Evaluates Expressions while the program is running recursively
evalExp :: [(String, Int)] -> Exp -> Int
evalExp bindings (BiggerThan e1 e2 ) =
  let eval = evalExp bindings in
    if eval e1 > eval e2
      then 1
      else 0
evalExp bindings (LessThan e1 e2) = 
   let eval = evalExp bindings in
    if eval e1 < eval e2
      then 1
      else 0

evalExp bindings (Number x) = x
evalExp bindings (Plus e1 e2) = (evalExp bindings (e1)) + (evalExp bindings (e2))
evalExp bindings (Minus e1 e2) = (evalExp bindings (e1)) - (evalExp bindings (e2))
evalExp bindings (Multiply e1 e2) = (evalExp bindings (e1)) * (evalExp bindings (e2))
evalExp bindings (Identifier s) = 
  let  x = (lookupAssoc s bindings) in 
  if x /= Nothing 
  then  removejust (lookupAssoc s bindings)
   else castError (VariableNotFound s)
-- Moves the robot across the board if the robot is going to be within bounds, else throws RobolError
evalMove :: Grid -> Start -> Statement -> [(String, Int)] -> (Exp, Exp)
evalMove (Size xsize ysize ) (Start (x,y)) (Move direction exp) bindings = 
                                              case direction of
                                              North -> if (checkbounds (evalExp bindings (Plus y exp)) (evalExp bindings ysize)) /= 1 then (x,Number (evalExp bindings (Plus y exp))) else castError  (OutOfBounds (x,y) )
                                              South -> if (checkbounds (evalExp bindings (Minus y exp)) (evalExp bindings ysize)) /= 1 then (x, Number (evalExp bindings (Minus y exp))) else castError (OutOfBounds (x,y))
                                              East -> if (checkbounds (evalExp bindings (Plus x exp)) (evalExp bindings xsize)) /= 1 then (Number (evalExp bindings (Plus x exp)), y ) else castError  (OutOfBounds (x,y))
                                              West -> if (checkbounds (evalExp bindings (Minus x exp)) (evalExp bindings xsize)) /= 1 then (Number (evalExp bindings (Minus x exp)),y) else (castError (OutOfBounds (x,y)))
--Assigns new value to variable                                             
evalAssignment :: Statement -> [(String, Int)] -> AssocList
evalAssignment  (Assignment (Identifier x) exp) bindings = change x (evalExp bindings exp) bindings
-- Loop which executes statements until a condition is met
evalLoop :: Statement -> [Statement] -> AssocList -> Statement -> [Statement]
evalLoop (Loop explist exp) statementlist bindings loopcopy = if (evalExp bindings (exp)) == 0 then (explist ++ loopcopy : statementlist) else statementlist


data RobolError = OutOfBounds (Exp,Exp) | VariableNotFound String


instance Show RobolError where show (VariableNotFound s) = show (VariableNotFound s)

-- Interprets the Statements and calls the required functions
interp :: Grid -> (Exp, Exp) -> AssocList -> [Statement] -> Either RobolError (Exp, Exp)
interp (Size xsize ysize) (x,y) bindings [] = Right (x,y)
interp (Size xsize ysize) (x,y) bindings (Stop:statmentlist) = Right (x,y)
interp (Size xsize ysize) (x,y) bindings ((Move direction exp):statmentlist) = interp (Size xsize ysize)  (evalMove (Size xsize ysize) (Start (x,y)) (Move direction exp) bindings)  bindings statmentlist
interp (Size xsize ysize) (x,y) bindings ((Assignment ident exp):statmentlist) = interp (Size xsize ysize)  (x,y)  (evalAssignment (Assignment ident exp) bindings) statmentlist
interp (Size xsize ysize) (x,y) bindings ((Loop explist exp):statmentlist) = interp (Size xsize ysize)  (x,y)  bindings (evalLoop (Loop explist exp) statmentlist bindings (Loop explist exp))

-- Removes the word Start and just keeps coordiantes and converts list of bindings to AssocList.
initial_interp :: Grid -> Start -> [Binding] -> [Statement] -> Either RobolError (Exp, Exp)
initial_interp (Size xsize ysize) (Start (x,y)) bindings statmentlist = interp (Size xsize ysize) (x,y) (convertbindtoassoc bindings []) statmentlist

-- Is called with the chosen program.
program_interpret :: Program -> Either RobolError (Exp, Exp)
program_interpret (Program (Size x y)  robot ) = initial_interp (Size x y) (robotStart robot) (robotBindings robot) (robotStatements robot)



testcode1 = Program (Size (Number 64) (Number 64)) Robot{robotBindings=[],
    robotStart = Start ((Number 23),(Number 30))
    ,robotStatements=[Move West (Number 15) ,
		Move South (Number 15) ,
		Move East (Plus (Number 2) (Number 3)) ,
		Move North (Plus (Number 10) (Number 27)),
		Stop
    ]
}
testcode2 = Program (Size (Number 64) (Number 64)) Robot{robotBindings = [Binding ((Identifier "i"),(Number 5)), Binding (( Identifier "j"), (Number 3))],
    robotStart= Start ((Number 23),(Number 6)) 
    ,robotStatements= [
		Move North (Multiply (Number 3) (Identifier "i")),
		Move East (Number 15) ,
		Move South (Minus (Minus (Number 12) (Identifier "i")) (Identifier "j")) ,
		Move West (Plus (Plus (Multiply (Number 2 ) (Identifier "i"))
								(Multiply (Number 3) (Identifier "j"))) (Number 1)),
		Stop
    ]
  }

testcode3 = Program (Size (Number 64) (Number 64)) Robot{robotBindings = [Binding ((Identifier "i"), (Number 5)), Binding ((Identifier "j"), (Number 3))],
    robotStart= Start ((Number 23),(Number 6)) 
    ,robotStatements= [
		Move North (Multiply (Number 3) (Identifier "i")),
		Move West (Number 15) ,
    Move East (Number 4) ,
    (Loop ([Move South (Identifier "j") , Assignment (Identifier "j") (Minus (Identifier "j") (Number 1))]) (LessThan (Identifier "j") (Number 1))),
		Stop
    ]
  }
testcode4 = Program (Size (Number 64) (Number 64)) Robot{robotBindings = [Binding((Identifier "i"),(Number 8))],
    robotStart= Start ((Number 1),(Number 1)) 
    ,robotStatements= [
    (Loop ([Move North (Identifier "i")]) (BiggerThan (Identifier "i") (Number 100))),
		Stop
    ]
  }



runcode x = case x of
            "1" -> program_interpret testcode1
            "2" -> program_interpret testcode2
            "3" -> program_interpret testcode3
            "4" -> program_interpret testcode4
            

allfunc = do
  print (program_interpret testcode1)
  print (program_interpret testcode2)
  print (program_interpret testcode3)
  print (program_interpret testcode4)

main = do
  putStrLn "Which Program to Run: "
  input1 <- getLine
  if "all" == input1 then allfunc else print (runcode  input1) 