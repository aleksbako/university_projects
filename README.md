# The University Assignments:

## The Projects that can be found here are:
    - Interpreter for a created language created by the university. I've written it both in Java and in Haskell.
    - Digital Image Processing Task of Cannies edge detection algorithm and performing different types of transformations on images and Lossy DCT on a different image.
    - OOP Tasks written in Java such as a hospital system, A task focusing on using threads in Java and a maze solver with a GUI which solves acyclic mazes.
    - Machine learning written in R evaluating different models and their performances.


### Interpreter
Written an interpreter of a made up languages. The syntax of the language is interpreted and performed in either java or Haskell. The commands are predetermined for simplicity purpouses.

### Digital Image Processing
    - Geometric and gray level transformations:
        1. Given an image the expected value and standard deviation of the image to a given value
        2. Position the input image to line up with face mask to be used for face recognition.
    - Cannies Algorithm:
        1. Perform Gauss filtering of input image with self implemented Convolution.
        2. Use symetric operator to find gradient of image in both axies.
        3. Find magnitude and gradient angles of edges.
        4. Perform thresholding given magnitude and angles.
        5. Perform hysteresis thresholding
    - Descrete Cosine Transformation:
        1. Perform DCT which is self implemented
        2. Use quantize matrix to quantize
        3. Evaluate image based images entropy.
        4. Perform IDCT on image .
        
Written in python using numpy and scipy in certain cases.
### OOP Tasks
    - Hospital task
        - Have a hospital system with patients and different types of doctors and medicine perscriptions. Use LinkedLists which are sorted and implement iterator to prioritize patients.
    - Thread Task
        - Read in a large amount of data of tested people and read their DNA, given specific DNA and if they tested positive you perform a binomial test the chance of a person being infected.
    - Maze:
        - Written in Swing, using recursion solve a maze and make it avoid acyclic paths. The maze is read in through a .txt file .

### Machine learning models
    - Analysis of both classification and regression models, performing cross validation to choose what model would fit best. Using different models such as linear regression, logistic regression, GAM models.
